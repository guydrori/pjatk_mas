package org.mas;

import java.util.List;

import org.mas.model.Person;

public interface PersonDao {

    List<Person> findAll();

    void addPerson(Person person);
}
