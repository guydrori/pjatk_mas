package pl.edu.pjwstk.s13997.mas.genericextentmanagement.associations;

import java.util.Objects;

public class Association {
    private Class<?> side1Type;
    private Class<?> side2Type;
    private AssociationType associationType;
    private int minMultiplicity;
    private Integer maxMultiplicity;

    public Association (Class<?> firstType, Class<?> secondType, AssociationType associationType,int multiplicity) {
        setFirstClass(firstType);
        setSecondClass(secondType);
        setAssociationType(associationType);
        setMinMultiplicity(multiplicity);
    }

    public Association (Class<?> firstType, Class<?> secondType, AssociationType associationType,int multiplicity,Integer maxMultiplicity) {
        setFirstClass(firstType);
        setSecondClass(secondType);
        setAssociationType(associationType);
        setMinMultiplicity(multiplicity);
        setMaxMultiplicity(maxMultiplicity);
    }

    public void setFirstClass(Class<?> firstClass) {
        if (firstClass == null) throw new NullPointerException();
        this.side1Type = firstClass;
    }

    public void setSecondClass(Class<?> secondClass) {
        if (secondClass == null) throw new NullPointerException();
        this.side2Type = secondClass;
    }

    public void setAssociationType(AssociationType type) {
        if (type == null) throw new NullPointerException();
        this.associationType = type;
    }

    public AssociationType getAssociationType() {
        return associationType;
    }

    public Class<?> getFirstClass() {
        return side1Type;
    }

    public Class<?> getSecondClass() {
        return side2Type;
    }

    public void setMinMultiplicity(int multiplicity) {
        if (multiplicity < 0) throw new IllegalArgumentException("Multiplicity can't be negative");
        this.minMultiplicity = multiplicity;
    }

    public int getMinMultiplicity() {
        return minMultiplicity;
    }

    public void setMaxMultiplicity(Integer maxMultiplicity) {
        if (maxMultiplicity < 0) throw new IllegalArgumentException("Multiplicity can't be negative");
        if (maxMultiplicity < minMultiplicity) throw new IllegalArgumentException("Max multiplicity can't be smaller than min multipilicity");
        this.maxMultiplicity = maxMultiplicity;
    }

    public Integer getMaxMultiplicity() {
        return maxMultiplicity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Association that = (Association) o;
        return minMultiplicity == that.minMultiplicity &&
                Objects.equals(side1Type, that.side1Type) &&
                Objects.equals(side2Type, that.side2Type) &&
                associationType == that.associationType &&
                Objects.equals(maxMultiplicity, that.maxMultiplicity);
    }

    @Override
    public int hashCode() {

        return Objects.hash(side1Type, side2Type, associationType, minMultiplicity, maxMultiplicity);
    }
}
