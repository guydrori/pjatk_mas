package pl.edu.pjwstk.s13997.mas.genericextentmanagement.base;

import pl.edu.pjwstk.s13997.mas.genericextentmanagement.associations.Association;
import pl.edu.pjwstk.s13997.mas.genericextentmanagement.associations.AssociationType;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

public abstract class ObjectBase<T extends Object> {
    private static Map<Class,Set<?>> extentMap= new HashMap<>();
    private static Set<Association> globalAssociations = new HashSet<>();
    protected Map<Association,List<Object>> associatedObjects;

    public ObjectBase() {
        Class currentClass = this.getClass();
        Set<T> extent = null;
        if (extentMap.containsKey(currentClass)) {
            extent = (Set<T>)extentMap.get(currentClass);
        } else {
            extent = new HashSet<>();
            extentMap.put(currentClass,extent);
        }
        associatedObjects = new HashMap<>();
        extent.add((T)this);
    }

    private static void removeGlobalAssociation(Association association) {
        globalAssociations.remove(association);
        for(Set<?> extent: extentMap.values()) {
            Set<ObjectBase> obExtent = (Set<ObjectBase>)extent;
            for (ObjectBase ob: obExtent) {
                ob.removeAssociatedObjects(association);
            }
        }
    }

    private void removeAssociatedObjects(Association association) {
        associatedObjects.remove(association);
    }

    public static void addAssociation(Association association,int othersideMinMultiplicity, Integer othersideMaxMultiplicity) {
        throw new IllegalStateException("Add association hasn't been implemented in the subclass");
    }

    public static void removeAssociation(Association association) {
        throw new IllegalStateException("Remove association hasn't been implemented in the subclass");
    }

    public void associateElement(ObjectBase object) {
        Optional<Association> association = associatedObjects.keySet().stream()
                .filter(a->a.getFirstClass() == this.getClass() && a.getSecondClass() == object.getClass()
                        || a.getFirstClass() == object.getClass() && a.getSecondClass() == this.getClass())
                .findFirst();
        if (!association.isPresent()) throw new IllegalArgumentException("No association for the class exists for the class of the given element");
        Association associationObject = association.get();
        if (associationObject.getAssociationType() == AssociationType.BINARY) {
            List<Object> associatedElements = associatedObjects.get(associationObject);
            if (associatedElements == null) throw new NullPointerException();
            if (associationObject.getMaxMultiplicity() != null && associatedElements.size() == associationObject.getMaxMultiplicity()) throw new IllegalArgumentException("Associating this element violates multiplicity constraints");
            if (!associatedElements.contains(object)) {
                associatedElements.add(object);
                associatedObjects.put(associationObject,associatedElements);
                object.associateElement(this);
            }
        }
    }

    public void unassociateElement(ObjectBase object) {
        Optional<Association> association = associatedObjects.keySet().stream()
                .filter(a->a.getFirstClass() == this.getClass() && a.getSecondClass() == object.getClass()
                        || a.getFirstClass() == object.getClass() && a.getSecondClass() == this.getClass())
                .findFirst();
        if (!association.isPresent()) throw new IllegalArgumentException("No association for the class exists for the class of the given element");
        Association associationObject = association.get();
        if (associationObject.getAssociationType() == AssociationType.BINARY) {
            List<Object> associatedElements = associatedObjects.get(associationObject);
            if (associatedElements == null) throw new NullPointerException();
            if (associatedElements.contains(object)) {
                associatedElements.remove(object);
                associatedObjects.put(associationObject,associatedElements);
                object.unassociateElement(this);
            }
        }
    }

    public static boolean containsAssociation(Association association) {
        throw new IllegalStateException("Contains association hasn't been implemented in the subclass");
    }

    public static boolean containsAssociation(Class class1, Class class2) {
        throw new IllegalStateException("Contains association hasn't been implemented in the subclass");
    }

    public Set<T> getExtent() {
        return new HashSet<T>((Set<T>)extentMap.get(this.getClass()));
    }

    public static void saveExtent(Class extentClass, String fileName) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileName));
            objectOutputStream.writeObject(extentMap.get(extentClass));
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent(Class extentClass,String fileName) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName));
            Set<?> extent = (Set<?>) objectInputStream.readObject();
            extentMap.put(extentClass,extent);
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showExtent(Class extentClass) throws ClassNotFoundException {
        Set<?> extent = null;
        if (extentMap.containsKey(extentClass)) { extent = extentMap.get(extentClass); }
        else { throw new ClassNotFoundException(); }
        System.out.println("The extent of class: " + extentClass.getSimpleName());
        for(Object instance : extent) {
            System.out.println(instance);
        }
    }

    public static void saveAllExtents() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("extents.dat"));
            objectOutputStream.writeObject(extentMap);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadAllExtents() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("extents.dat"));
            extentMap = (HashMap<Class,Set<?>>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
