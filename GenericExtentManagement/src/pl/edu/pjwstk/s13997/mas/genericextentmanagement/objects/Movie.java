package pl.edu.pjwstk.s13997.mas.genericextentmanagement.objects;

import pl.edu.pjwstk.s13997.mas.genericextentmanagement.associations.Association;
import pl.edu.pjwstk.s13997.mas.genericextentmanagement.base.ObjectBase;

import java.util.HashSet;
import java.util.Set;

public class Movie extends ObjectBase<Movie> {
    private static Set<Association> associations  = new HashSet<>();
    public static void addAssociation(Association association,int othersideMinMultiplicity, Integer othersideMaxMultiplicity) {
        throw new IllegalStateException("Add association hasn't been implemented in the subclass");
    }

    public static void removeAssociation(Association association) {
        if (associations.contains(association)) {
            associations.remove(association);

        }
    }

    public static boolean containsAssociation(Association association) {
       return associations.contains(association);
    }

    public static boolean containsAssociation(Class class1, Class class2) {
        return associations.stream().anyMatch(a-> {
            boolean variant1 = a.getFirstClass().equals(class1) && a.getSecondClass().equals(class2);
            boolean variant2 = a.getFirstClass().equals(class2) && a.getSecondClass().equals(class1);
            return variant1 || variant2;
        });
    }
}
