package pl.edu.pjwstk.s13997.mas.genericextentmanagement.associations;

public enum AssociationType {
    BINARY,
    QUALIFIED,
    COMPOSITION
}
