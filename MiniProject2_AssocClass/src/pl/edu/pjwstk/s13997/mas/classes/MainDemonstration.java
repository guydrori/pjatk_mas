package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.classes.employee.EmployeeBuilder;

public class MainDemonstration {
    public static void main(String[] args) {
        //Creating sample data with a complex attribute
        Company company = new Company(3627967698L,"Testowa Firma Sp. z o.o.",new Address("Marszałkowska 1","Warszawa","00-000"));

        //Example of a binary association
        Department department = new Department("Accounting","Poland");
        department.setCompany(company);
        System.out.println("Company departments: " + company.getDepartments());
        System.out.println("Department company: " + department.getCompany());

        //Example of a qualified association
        Position position = new Position("Independent accountant");
        Employee employee = EmployeeBuilder.build("ABC001","John","Doe",company,department,position);
        System.out.println("\nEmployee companies: " + employee.getCompanies());
        System.out.println("Company employees: " + company.getEmployees());

        //Example of a composition:
        System.out.println("\nEmployee department: " + employee.getDepartment());
        System.out.println("Department employees: " + department.getEmployees());

        //Example of an association with attributes
        PersonalDataStore dataStore = new PersonalDataStore("E-commerce","Admin 1:\nJohn Doe\nIT Manager\nTestowa Firma Sp. z o.o.\nj.doe@testowafirma.pl","Customer data required for realizing online orders","All e-commerce clients","Name, Address, Email","SSL Encryption, server firewall");
        PositionPermissions permissions = new PositionPermissions(dataStore,position,Position.Permissions.READ);
        dataStore.addDefaultPermissions(permissions);
        System.out.println("\nPosition permissions: " + position.getDefaultPermissions());
        System.out.println("Personal data store permissions: " + dataStore.getDefaultPermissionSet());
    }
}
