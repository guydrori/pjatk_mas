package pl.edu.pjwstk.s13997.mas.classes;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class User implements Serializable {
    private static int idCounter = 1;
    private int id;
    private String username;
    private byte[] password;
    private byte[] salt;
    private static Set<User> extent = new HashSet<>();

    public User(int id, String username, String password) {
        setId(id);
        setPassword(password);
        setUsername(username);
        addToExtent(this);
    }

    public User(String username, String password) {
        this.id = idCounter;
        idCounter++;
        setPassword(password);
        setUsername(username);
        addToExtent(this);
    }

    private void setId(int id) {
        if (id >= idCounter) {
            this.id = id;
            if (id == idCounter) idCounter++;
            else idCounter = id + 1;
        } else {
            throw new IllegalArgumentException("The given ID has been already assigned or it is invalid");
        }
    }

    public int getId() {
        return id;
    }

    public void setUsername(String username) {
        if (username == null || username.isEmpty()) throw new IllegalArgumentException("A username must be given");
        if (extent.stream().anyMatch(u-> u.username.equals(username))) throw new IllegalArgumentException("The given username is used");
        this.username = username;
    }

    public String getUsername() { return username; }

    public static byte[] generateSalt() throws NoSuchAlgorithmException{
        // VERY important to use SecureRandom instead of just Random
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

        // Generate a 8 byte (64 bit) salt as recommended by RSA PKCS5
        byte[] salt = new byte[8];
        random.nextBytes(salt);

        return salt;
    }

    public static byte[] hashPassword(String password, byte[] salt) throws NoSuchAlgorithmException,InvalidKeySpecException {
        // PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
        // specifically names SHA-1 as an acceptable hashing algorithm for PBKDF2
        String algorithm = "PBKDF2WithHmacSHA1";
        // SHA-1 generates 160 bit hashes, so that's what makes sense here
        int derivedKeyLength = 160;
        // Pick an iteration count that works for you. The NIST recommends at
        // least 1,000 iterations:
        // http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
        // iOS 4.x reportedly uses 10,000:
        // http://blog.crackpassword.com/2010/09/smartphone-forensics-cracking-blackberry-backup-passwords/
        int iterations = 20000;

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);

        SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);

        return f.generateSecret(spec).getEncoded();
    }

    public void setPassword(String password) {
        if (password == null || password.isEmpty()) throw new IllegalArgumentException("A password must be given");
        Boolean unique = false;
        while (!unique) {
            try {
                this.salt = generateSalt();
                if (extent.stream().anyMatch(u->Arrays.equals(u.salt,this.salt))) unique = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        unique = false;
        while (!unique) {
            try {
                this.password = hashPassword(password,this.salt);
                if (extent.stream().anyMatch(u->Arrays.equals(u.password,this.password))) unique = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public byte[] getPassword() { return password; }

    public byte[] getSalt() { return salt; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                Objects.equals(username, user.username) &&
                Arrays.equals(password, user.password) &&
                Arrays.equals(salt, user.salt);
    }

    private static void addToExtent(User user) {
        if (User.extent.contains(user)) throw new IllegalArgumentException("An identical user exists!");
        User.extent.add(user);
    }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("users.dat"));
            objectOutputStream.writeObject(User.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("users.dat"));
            User.extent = (Set<User>)objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }

}