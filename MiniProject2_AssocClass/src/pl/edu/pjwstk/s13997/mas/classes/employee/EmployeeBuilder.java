package pl.edu.pjwstk.s13997.mas.classes.employee;

import pl.edu.pjwstk.s13997.mas.classes.Company;
import pl.edu.pjwstk.s13997.mas.classes.Department;
import pl.edu.pjwstk.s13997.mas.classes.Position;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

public class EmployeeBuilder {
    public static Employee build(String id, String firstName, String surname, Map<Long,Company> companies,Department department,Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        Employee newEmployee = new Employee(id,firstName,surname,companies,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }

    public static Employee build(String id, String firstName, String surname,Company company, Department department,Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        Employee newEmployee = new Employee(id,firstName,surname,company,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }

    public static Employee build(String id, String firstName, String surname, Collection<LocalDate> trainingDates, Company company, Department department,Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        Employee newEmployee = new Employee(id,firstName,surname,trainingDates,company,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }
}
