package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class Employment {
    private Company company;
    private Employee employee;
    private LocalDate startDate;
    private LocalDate endDate;
    private static Set<Map.Entry<Company,Employee>> pairSet = new HashSet<>();
    private static Set<Employment> extent = new HashSet<>();
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-YYYY");

    public Employment(Company company, Employee employee, LocalDate startDate, LocalDate endDate) {
        setEmploymentDates(startDate,endDate);
        setCompany(company);
        setEmployee(employee);
        Map.Entry<Company,Employee> entry = new Map.Entry<>() {
            @Override
            public Company getKey() {
                return company;
            }

            @Override
            public Employee getValue() {
                return employee;
            }

            @Override
            public Employee setValue(Employee value) {
                return null;
            }
        };
        extent.add(this);
        pairSet.add(entry);
    }

    public Employment(Company company, LocalDate startDate, LocalDate endDate) {
        setEmploymentDates(startDate,endDate);
        setCompany(company);
        Map.Entry<Company,Employee> entry = new Map.Entry<>() {
            @Override
            public Company getKey() {
                return company;
            }

            @Override
            public Employee getValue() {
                return employee;
            }

            @Override
            public Employee setValue(Employee value) {
                return null;
            }
        };
        extent.add(this);
        pairSet.add(entry);
    }

    private void setCompany(Company company) {
        if (company == null) throw new NullPointerException("Company cannot be null!");
        this.company = company;
    }

    public void setEmployee(Employee employee) {
        if (this.employee != null) throw new RuntimeException("An employee is already asssigned!");
        if (employee == null) throw new NullPointerException("Employee cannot be null");
        this.employee = employee;
        if (!employee.containsEmploymentRecord(this)) {
            employee.addEmploymentRecord(this);
        }
    }

    public Company getCompany() {
        return company;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setStartDate(LocalDate startDate) {
        if (startDate == null) throw new NullPointerException();
        if (startDate.isEqual(endDate) || startDate.isAfter(endDate)) throw new IllegalArgumentException("The start date cannot be equal to the end date or be after it!");
        this.startDate = startDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setEndDate(LocalDate endDate) {
        if (endDate == null) {
            this.endDate = null;
            return;
        }
        if (endDate.isEqual(startDate) || endDate.isBefore(startDate))
            throw new IllegalArgumentException("The end date cannot be equal to the end date or be before it!");
        this.endDate = endDate;
    }

    public void setEmploymentDates(LocalDate startDate, LocalDate endDate) {
        if (startDate == null) throw new NullPointerException();
        if (endDate != null) {
            if (startDate.isEqual(endDate) || startDate.isAfter(endDate)) throw new IllegalArgumentException("The start date cannot be equal to the end date or be after it!");
            if (endDate.isEqual(startDate) || endDate.isBefore(startDate))
                throw new IllegalArgumentException("The end date cannot be equal to the end date or be before it!");
        }
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public LocalDate getEndDate() { return endDate; }


    public static Set<Employee> getEmployees(Company company) {
        return pairSet.stream().filter(e->e.getKey().equals(company)).map(Map.Entry::getValue).collect(Collectors.toSet());
    }

    public static Set<Employment> getEmploymentRecords(Employee employee) {
        return extent.stream().filter(emp->emp.getEmployee().equals(employee)).collect(Collectors.toSet());
    }

    public static boolean pairExists(Company company, Employee employee) {
        return pairSet.contains(new Map.Entry<Company, Employee>() {
            @Override
            public Company getKey() {
                return company;
            }

            @Override
            public Employee getValue() {
                return employee;
            }

            @Override
            public Employee setValue(Employee value) { return null; }
        });
    }

    public static void remove(Employment employment) {
        extent.remove(employment);
        pairSet.remove(new Map.Entry<Company, Employee>() {
            @Override
            public Company getKey() {
                return employment.company;
            }

            @Override
            public Employee getValue() {
                return employment.employee;
            }

            @Override
            public Employee setValue(Employee value) { return null; }
        });
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Company: ").append(company.getName()).append(", Employee: ").append(employee.getFullName()).append(", Dates: Start: ").append(startDate.format(DATE_FORMATTER));
        if (endDate != null) {
            stringBuilder.append(" End: ").append(endDate.format(DATE_FORMATTER));
        }
        return stringBuilder.toString();
    }
}
