package pl.edu.pjwstk.s13997.mas.classes;

import java.util.HashSet;
import java.util.Set;

public class Attachment {
    //XOR
    private PersonalDataStore personalDataStore;
    private SecurityBreach securityBreach;
    private String fileName;
    private String URL;
    private static Set<Attachment> extent = new HashSet<>();

    public Attachment(SecurityBreach securityBreach, String fileName, String URL) {
        setFileName(fileName);
        setSecurityBreach(securityBreach);
        setURL(URL);
        extent.add(this);
    }

    public Attachment(PersonalDataStore personalDataStore, String fileName, String URL) {
        setFileName(fileName);
        setPersonalDataStore(personalDataStore);
        setURL(URL);
        extent.add(this);
    }

    public void setFileName(String fileName) {
        if(fileName == null || fileName.isEmpty()) throw new IllegalArgumentException("A file name must be provided");
        if (extent.stream().anyMatch(u->u.fileName.equals(fileName))) throw new IllegalArgumentException("An attachment exists with the given filename");
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setURL(String URL) {
        if(URL == null || URL.isEmpty()) throw new IllegalArgumentException("A URL must be provided");
        if (extent.stream().anyMatch(u->u.URL.equals(URL))) throw new IllegalArgumentException("An attachment exists with the given URL");
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }

    public PersonalDataStore getPersonalDataStore() {
        return personalDataStore;
    }

    public void setPersonalDataStore(PersonalDataStore personalDataStore) {
        if (personalDataStore == null) {
            this.personalDataStore = null;
            return;
        }
        if (securityBreach != null) throw new IllegalArgumentException("This attachment is already assigned to a security breach!");
        if (this.personalDataStore != null) {
            this.personalDataStore.removeAttachment(this);
        }
        if (!personalDataStore.contains(this)) {
            personalDataStore.addAttachment(this);
        }
        this.personalDataStore = personalDataStore;
    }

    public boolean canAssignPersonalDataStore() {
        return securityBreach == null;
    }

    public SecurityBreach getSecurityBreach() {
        return securityBreach;
    }

    public void setSecurityBreach(SecurityBreach securityBreach) {
        if (securityBreach == null) {
            this.securityBreach = null;
            return;
        }
        if (personalDataStore != null) throw new IllegalArgumentException("This attachment is already assigned to a personal data store!");
        if (this.securityBreach != null) {
            this.securityBreach.removeAttachment(this);
        }
        if (!securityBreach.contains(this)) {
            securityBreach.addAttachment(this);
        }
        this.securityBreach = securityBreach;
    }

    public boolean canAssignSecurityBreach() {
        return personalDataStore == null;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("File name: ").append(getFileName()).append(" URL: ").append(getURL());
        return stringBuilder.toString();
    }
}
