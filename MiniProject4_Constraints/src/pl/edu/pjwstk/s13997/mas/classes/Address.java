package pl.edu.pjwstk.s13997.mas.classes;

import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Pattern;

//Complex attribute
public class Address implements Serializable {
    private static final String POSTCODE_PATTERN = "\\d{2}-\\d{3}";
    private String streetData;
    private String city;
    private String postCode;

    public Address(String streetData, String city, String postCode) {
        setPostCode(postCode);
        setCity(city);
        setStreetData(streetData);
    }

    public void setPostCode(String postCode) {
        if (postCode == null || postCode.isEmpty()) throw new IllegalArgumentException("A postcode must be provided!");
        if (!Pattern.matches(POSTCODE_PATTERN,postCode)) throw new IllegalArgumentException("The given postcode must follow the Polish format: 00-000");
        this.postCode = postCode;
    }

    public String getPostCode() { return postCode; }

    public void setCity(String city) {
        if (city == null || city.isEmpty()) throw new IllegalArgumentException("A city must be provided!");
        this.city = city.toUpperCase(Locale.forLanguageTag("pl-PL"));
    }

    public String getCity() { return city; }

    public void setStreetData (String streetData) {
        if (streetData == null || streetData.isEmpty())  throw new IllegalArgumentException("Street data must be provided!");
        this.streetData = streetData.toUpperCase(Locale.forLanguageTag("pl-PL"));
    }

    public String getStreetData() { return streetData; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(streetData, address.streetData) &&
                Objects.equals(city, address.city) &&
                Objects.equals(postCode, address.postCode);
    }
}
