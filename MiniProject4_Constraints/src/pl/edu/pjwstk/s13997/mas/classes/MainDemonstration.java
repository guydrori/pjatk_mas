package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.classes.employee.EmployeeBuilder;

import java.time.LocalDate;

public class MainDemonstration {
    public static void main(String[] args) {
        //Example of unique constraint
        Company company = new Company(3627967698L,"Testowa Firma Sp. z o.o.",new Address("Marszałkowska 1","Warszawa","00-000"));
        try {
            Company company2 = new Company(3627967698L,"Testowa Firma Sp. z o.o.",new Address("Marszałkowska 1","Warszawa","00-000"));
        } catch (IllegalArgumentException e) {
            if (e.getMessage().equals("A company with the given NIP exists")) System.out.println("Successfully blocked the creation of another company with the same NIP");
        }

        //Example of bag
        Department department = new Department("Accounting","Poland");
        department.setCompany(company);
        Position position = new Position("Independent accountant");
        Employment employmentRecord1 = new Employment(company,LocalDate.of(2017,06,01),null);
        Employee employee = EmployeeBuilder.build("ABC001","John","Doe",employmentRecord1,department,position);
        Employment employmentRecord2 = new Employment(company,employee,LocalDate.of(2011,02,01),LocalDate.of(2012,03,31));
        System.out.println("\nEmployee employment record: " + employee.getEmploymentRecords());
        System.out.println("Company employees: " + company.getEmploymentRecords());

        //Example of ordered constraint
        Employment employmentRecord3 = new Employment(company,LocalDate.of(2017,07,01),null);
        Employee employee2 = EmployeeBuilder.build("ABC002","Anne","Doe",employmentRecord3,department,position);
        System.out.println("\nOrdered extent: " + Employee.exportList());

        //Example of XOR
        PersonalDataStore dataStore = new PersonalDataStore("E-commerce","Admin 1:\nJohn Doe\nIT Manager\nTestowa Firma Sp. z o.o.\nj.doe@testowafirma.pl","Customer data required for realizing online orders","All e-commerce clients","Name, Address, Email","SSL Encryption, server firewall");
        PositionPermissions permissions = new PositionPermissions(dataStore,position,Position.Permissions.READ);
        dataStore.addDefaultPermissions(permissions);
        Attachment attachment1 = new Attachment(dataStore,"logo-lg-md.png","http://www.pja.edu.pl/templates/pjwstk/images/logo-lg-md.png");
        System.out.println("\nPersonal data store attachments: " + dataStore.getAttachments());
        SecurityBreach securityBreach = new SecurityBreach("E-commerce breach","Customer data leak","Increased server security",SecurityBreach.Status.SIGNIFICANT,dataStore);
        try {
            securityBreach.addAttachment(attachment1);
        } catch (IllegalArgumentException e) {
            if (e.getMessage().equals("This attachment is already assigned to a personal data store!")) System.out.println("\nSuccessfully blocked the assignment of an attachment to a security breach");
        }
    }
}
