package pl.edu.pjwstk.s13997.mas.classes;

import java.io.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Role implements Serializable {
    private static int idCounter = 1; //Class attribute
    private int id;
    private String name;
    public enum Permissions {READ,ADMIN,REPORTS_ONLY,EDIT}; //Class attribute
    private Permissions permissions;
    private static Set<Role> extent = new HashSet<>(); //Class extent

    public Role (int id, String name, Permissions permissions) {
        setPermissions(permissions);
        setName(name);
        setId(id);
        addToExtent(this);
    }

    public Role (String name, Permissions permissions) {
        setPermissions(permissions);
        setName(name);
        this.id = idCounter;
        idCounter++;
        addToExtent(this);
    }

    private void setId(int id) {
        if (id >= idCounter) {
            this.id = id;
            if (id == idCounter) idCounter++;
            else idCounter = id + 1;
        } else {
            throw new IllegalArgumentException("The given ID has been already assigned or it is invalid");
        }
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("A name must be given!");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPermissions(Permissions permissions) {
        if (permissions == null) throw new NullPointerException("Permissions can't be null");
        if (extent.stream().anyMatch(r -> r.permissions == permissions)) throw new IllegalArgumentException("A given set of permissions can only appear once");
        this.permissions = permissions;
    }

    public Permissions getPermissions() {
        return permissions;
    }

    private static void addToExtent(Role role) {
        if (Role.extent.contains(role)) throw new IllegalArgumentException("An identical role exists!");
        Role.extent.add(role);
    }

    //Class extent persistence
    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("roles.dat"));
            objectOutputStream.writeObject(Role.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("roles.dat"));
            Role.extent = (Set<Role>)objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }

    //Method overriding
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return id == role.id &&
                Objects.equals(name, role.name) &&
                permissions == role.permissions;
    }
}
