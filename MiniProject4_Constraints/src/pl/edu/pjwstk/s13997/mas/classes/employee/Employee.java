package pl.edu.pjwstk.s13997.mas.classes.employee;

import pl.edu.pjwstk.s13997.mas.classes.*;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class Employee implements Serializable {
    private String id;
    private String firstName;
    private String surname;
    private HashMap<Long,Set<Employment>> employmentRecords; //Bag
    private Department department;
    private Department managedDepartment;
    private List<LocalDate> trainingDates; //Multi-value attribute
    private Position position;
    private Double salary;
    private Set<SecurityBreach> securityBreaches;
    private static Set<SecurityBreach> allAssociatedSecurityBreaches = new HashSet<>();
    private static Set<Employee> extent = new TreeSet<>(Comparator.comparing(Employee::getId));

    Employee(String id, String firstName, String surname,Map<Long,Set<Employment>> employmentRecords,Department department, Position position,Collection<SecurityBreach> securityBreaches) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        trainingDates = new LinkedList<>();
        setEmploymentRecords(employmentRecords);
        this.department = department;
        setPosition(position);
        setSecurityBreaches(securityBreaches);
        extent.add(this);
    }

    Employee(String id, String firstName, String surname,Map<Long,Set<Employment>> employmentRecords,Department department, Position position) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        trainingDates = new LinkedList<>();
        setEmploymentRecords(employmentRecords);
        this.department = department;
        setPosition(position);
        securityBreaches = new HashSet<>();
        extent.add(this);
    }

    Employee(String id, String firstName, String surname,Employment employmentRecord,Department department, Position position) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        trainingDates = new LinkedList<>();
        setEmploymentRecord(employmentRecord);
        this.department = department;
        setPosition(position);
        securityBreaches = new HashSet<>();
        extent.add(this);
    }

    Employee(String id, String firstName,String surname, Collection<LocalDate> trainingDates,Employment employmentRecord, Department department, Position position) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        if (trainingDates == null) throw new NullPointerException("Training dates collection can't be null");
        this.trainingDates = new LinkedList<>(trainingDates);
        setEmploymentRecord(employmentRecord);
        this.department = department;
        setPosition(position);
        securityBreaches = new HashSet<>();
        extent.add(this);
    }

    private void setId(String id) {
        if(id == null || id.isEmpty()) throw new IllegalArgumentException("An ID must be provided");
        if(extent.stream().anyMatch(e->e.id.equals(id))) throw new IllegalArgumentException("An employee with the given ID exists");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        if(firstName == null || firstName.isEmpty()) throw new IllegalArgumentException("A first name must be provided");
        this.firstName = firstName;
    }

    private String getFirstName() { return firstName; }

    public void setSurname(String surname) {
        if(surname == null || surname.isEmpty()) throw new IllegalArgumentException("A first name must be provided");
        this.surname = surname;
    }

    private String getSurname() { return  surname; }

    //Derived attribute
    public String getFullName() { return firstName + " " + surname; }

    //Derived attribute
    public LocalDate getMostRecentTrainingDate() {
        if (trainingDates != null) return trainingDates.stream().max((d1,d2)-> {return d1.compareTo(d2);}).get();
        else return null;
    }

    public List<LocalDate> getTrainingDates() {
        if (trainingDates != null) return new LinkedList<>(trainingDates);
        else return null;
    }

    public void addTrainingDate(LocalDate date) {
        if (trainingDates == null) trainingDates = new LinkedList<>();
        trainingDates.add(date);
    }

    private void setEmploymentRecords(Map<Long,Set<Employment>> employmentRecords) {
        this.employmentRecords = new HashMap<>(employmentRecords);
    }

    public Map<Long,Set<Employment>> getEmploymentRecords() {
        return new HashMap<>(employmentRecords);
    }

    public void addEmploymentRecord(Employment employmentRecord) {
        if (employmentRecord == null) throw new NullPointerException("Employment record musn't be null!");
        if (employmentRecord.getEmployee() != this) throw new IllegalArgumentException("The given record is for another employee");
        if (!employmentRecord.getCompany().containsEmploymentRecord(employmentRecord)) {
            employmentRecord.getCompany().addEmploymentRecord(employmentRecord);
        }
        if (employmentRecords.containsKey(employmentRecord.getCompany().getNIP())) {
            Set<Employment> records = employmentRecords.get(employmentRecord.getCompany().getNIP());
            records.add(employmentRecord);
            employmentRecords.put(employmentRecord.getCompany().getNIP(),records);
        } else {
            Set<Employment> records = new TreeSet<>(Comparator.comparing(Employment::getStartDate));
            records.add(employmentRecord);
            employmentRecords.put(employmentRecord.getCompany().getNIP(),records);
        }
    }

    public Set<Employment> getCompanyRecords(Long nip) {
        if (nip == null || nip < 0) throw new NullPointerException("A NIP value must be provided!");
        return employmentRecords.getOrDefault(nip,null);
    }

    public boolean containsCompany(Long nip) {
        return employmentRecords.containsKey(nip);
    }

    public boolean containsEmploymentRecord(Employment employmentRecord) {
        return employmentRecords.values().stream().anyMatch(er-> er.contains(employmentRecord));
    }

    private void setEmploymentRecord(Employment employmentRecord) {
        if (employmentRecords == null) employmentRecords = new HashMap<>();
        if (employmentRecord == null) throw new NullPointerException("Employment record musn't be null!");
        if (employmentRecord.getEmployee() != null && employmentRecord.getEmployee() != this) throw new IllegalArgumentException("The given record is for another employee");
        if (employmentRecord.getEmployee() == null) {
            employmentRecord.setEmployee(this);
        }
        Set<Employment> records;
        if (employmentRecords.containsKey(employmentRecord.getCompany().getNIP())) {records = employmentRecords.get(employmentRecord.getCompany().getNIP()); }
        else { records = new TreeSet<>(Comparator.comparing(Employment::getStartDate));}
        if (employmentRecord.getCompany().containsEmploymentRecord(employmentRecord)) {
            employmentRecord.getCompany().addEmploymentRecord(employmentRecord);
        }
        records.add(employmentRecord);
        employmentRecords.put(employmentRecord.getCompany().getNIP(),records);
    }

    public void removeEmploymentRecord(Employment employmentRecord) {
        if (!employmentRecords.containsKey(employmentRecord.getCompany().getNIP())) throw new IllegalArgumentException("No records for ther given company exist");
        if (!employmentRecords.get(employmentRecord.getCompany().getNIP()).contains(employmentRecord)) throw new IllegalArgumentException("No records for ther given company exist");
        if (employmentRecord.getCompany().containsEmploymentRecord(employmentRecord)) {
            employmentRecord.getCompany().removeEmploymentRecord(employmentRecord);
        }
        employmentRecords.get(employmentRecord.getCompany().getNIP()).remove(employmentRecord);
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (this.department != null) {
            this.department.removeEmployee(this,department);
            //if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        } else {
            department.addEmployee(this);
        }
        this.department = department;
    }

    public void setManagedDepartment(Department department) {
        if (department == null) throw new NullPointerException();
        if (this.department == null) throw new NullPointerException("This Employee is not associated with any department!!");
        if (department != this.department) throw new IllegalArgumentException("The given department is different than the one this Employee belongs to!");
        this.managedDepartment = department;
        if (department.getManager() != this) {
            department.setManager(this);
        }
    }

    public Department getManagedDepartment() {
        return managedDepartment;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        if (position == null) throw new NullPointerException("Position can't be null");
        if (this.position != null) {
            this.position.removeEmployee(this);
        }
        this.position = position;
        position.addEmployee(this);
    }

    private void setSecurityBreaches(Collection<SecurityBreach> securityBreaches) {
        if (securityBreaches == null) throw new NullPointerException("The security breaches collection can't be null!");
        this.securityBreaches = new HashSet<>(securityBreaches);
        allAssociatedSecurityBreaches.addAll(securityBreaches);
    }

    public Set<SecurityBreach> getSecurityBreaches() {
        return new HashSet<>(securityBreaches);
    }

    public void addSecurityBreach(SecurityBreach securityBreach) {
        if (securityBreach == null) return;
        if (!securityBreach.canAssociateEmployee()) throw new RuntimeException("This security breach must be associated to a personal data store before assigning an employee!!");
        if (allAssociatedSecurityBreaches.contains(securityBreach)) throw new IllegalArgumentException("The given security breach is already associated to an employee");
        this.securityBreaches.add(securityBreach);
        allAssociatedSecurityBreaches.add(securityBreach);
    }

    public void removeSecurityBreach(SecurityBreach securityBreach) {
        if (securityBreach == null) throw new NullPointerException("Security breach can't be null");
        if (allAssociatedSecurityBreaches.contains(securityBreach) && !securityBreaches.contains(securityBreach)) throw new IllegalArgumentException("Wrong employee");
        this.securityBreaches.remove(securityBreach);
        allAssociatedSecurityBreaches.remove(securityBreach);
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        if (salary == null) throw new NullPointerException();
        if (this.salary == null) {
            if (salary < 2500) throw new IllegalArgumentException("Salary must be greater than 2,500!");
        } else {
            double fivePercent = 0.05 * this.salary;
            double salaryDiff = salary - this.salary;
            if (salaryDiff < 0 || salaryDiff > fivePercent) throw new IllegalArgumentException("A salary can only be increased up to 5%!");
            this.salary = salary;
        }
    }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("employees.dat"));
            objectOutputStream.writeObject(Employee.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("employees.dat"));
            Employee.extent = (Set<Employee>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printTrainingReport() {
        System.out.println("ID\tFirst name\tSurname\tMost recent training date\n");
        extent.forEach(e-> System.out.println(e.getId()+"\t"+e.getFirstName()+"\t"+e.getSurname()+"\t"+e.getMostRecentTrainingDate()));
    }

    public static List<Employee> exportList() {
        return new ArrayList<>(extent);
    }

    @Override
    public String toString() {
        return getFullName() +" " + getId();
    }

}
