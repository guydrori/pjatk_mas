package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Department implements Serializable {
    private static int idCounter = 1;
    private int id;
    private String name;
    private String region;
    private Company company;
    private Employee manager;
    private Set<Employee> employees;
    private static Set<Department> extent = new HashSet<>();

    public Department(int id, String name, String region, Company company, Collection<Employee> employees) {
        setName(name);
        setRegion(region);
        setId(id);
        setCompany(company);
        setEmployees(employees);
        extent.add(this);
    }

    public Department(int id, String name,String region,Company company) {
        setName(name);
        setRegion(region);
        setId(id);
        setCompany(company);
        employees = new HashSet<>();
        extent.add(this);
    }

    public Department(String name,String region) {
        setName(name);
        setRegion(region);
        id = idCounter;
        idCounter++;
        employees = new HashSet<>();
        extent.add(this);
    }

    private void setId(int id) {
        if (id >= idCounter) {
            this.id = id;
            if (id == idCounter) idCounter++;
            else idCounter = id + 1;
        } else {
            throw new IllegalArgumentException("The given ID has been already assigned or it is invalid");
        }
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        if(name == null || name.isEmpty()) throw new IllegalArgumentException("A name must be provided");
        if (extent.stream().anyMatch(d->d.name.equals(name))) throw new IllegalArgumentException("A department with the given name exists");
        this.name = name;
    }

    public String getName() { return name; }

    public void setRegion(String region) {
        if(region == null || region.isEmpty()) throw new IllegalArgumentException("A region must be provided");
        this.region = region;
    }

    public String getRegion() { return region; }

    public void setCompany(Company company) {
        if (company == null) throw new NullPointerException("Company must not be null!");
        if (this.company != null) {
            this.company.removeDepartment(this);
        }
        this.company = company;
        company.addDepartment(this);
    }

    public Company getCompany() {
        return company;
    }

    private void setEmployees(Collection<Employee> employees) {
        if (employees == null) throw new NullPointerException();
        this.employees = new HashSet<>(employees);
    }

    public void addEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee cannot be null!");
        if (anyContains(employee.getId())) throw new IllegalArgumentException("An employee with the given ID already works in a department");
        employees.add(employee);
        if (employee.getDepartment() == null || employee.getDepartment() != this) {
            employee.setDepartment(this);
        }
    }

    public void removeEmployee(Employee employee,Department newDepartment) {
        if (employee == null) throw new NullPointerException("Employee cannot be null!");
        if (newDepartment == null) throw new NullPointerException("New department can't be null!");
        employees.remove(employee);
        if (!newDepartment.contains(employee)) {
            newDepartment.addEmployee(employee);
        }
    }

    public void setManager(Employee manager) {
        if (manager == null) throw new NullPointerException();
        if (manager.getDepartment() == null) throw new NullPointerException("The given Employee is not assigned to any department!!");
        if (manager.getDepartment() != this || !employees.contains(manager)) throw new IllegalArgumentException("The given Employee doesn't belong to this department!");
        this.manager = manager;
        if (manager.getManagedDepartment() == null || manager.getManagedDepartment() != this) {
            manager.setManagedDepartment(this);
        }
    }

    public Employee getManager() {
        return manager;
    }

    public Set<Employee> getEmployees() {
        return new HashSet<>(employees);
    }

    public boolean contains(Employee employee) {
        return employees.contains(employee);
    }

    public static boolean anyContains(String id) {
        return extent.stream().anyMatch(d->{
            if (d.employees != null) return d.employees.stream().anyMatch(e->e.getId().equals(id));
            else return false;
        });
    }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("depts.dat"));
            objectOutputStream.writeObject(Department.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("depts.dat"));
            Department.extent = (Set<Department>)objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }

    @Override
    public String toString() {
        return "Company: " + company.getName() + ", Name: " + getName();
    }
}
