package pl.edu.pjwstk.s13997.mas.classes.employee;

import pl.edu.pjwstk.s13997.mas.classes.*;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class EmployeeBuilder {
    public static Employee build(String id, String firstName, String surname, Map<Long,Set<Employment>> employmentRecords, Department department, Position position, Collection<SecurityBreach> securityBreaches) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        Employee newEmployee = new Employee(id,firstName,surname,employmentRecords,department,position,securityBreaches);
        department.addEmployee(newEmployee);
        return newEmployee;
    }

    public static Employee build(String id, String firstName, String surname, Map<Long,Set<Employment>> employmentRecords,Department department,Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        Employee newEmployee = new Employee(id,firstName,surname,employmentRecords,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }

    public static Employee build(String id, String firstName, String surname,Employment employmentRecord, Department department,Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        Employee newEmployee = new Employee(id,firstName,surname,employmentRecord,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }

    public static Employee build(String id, String firstName, String surname, Collection<LocalDate> trainingDates, Employment employmentRecord, Department department,Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        Employee newEmployee = new Employee(id,firstName,surname,trainingDates,employmentRecord,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }
}
