package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Company implements Serializable {
    private long NIP;
    private String name;
    private Address address; //Complex attribute
    private Set<Department> departments;
    private Set<Employment> employmentRecords;
    private static Set<Company> extent = new HashSet<>();

    public Company(long NIP, String name, Address address, Collection<Department> departments) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        setDepartments(departments);
        employmentRecords = new HashSet<>();
        extent.add(this);
    }

    public Company(long NIP, String name, Address address, Collection<Department> departments,Collection<Employment> employmentRecords) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        setDepartments(departments);
        setEmploymentRecords(employmentRecords);
        extent.add(this);
    }

    public Company(long NIP, String name, Address address) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        departments = new HashSet<>();
        employmentRecords = new HashSet<>();
        extent.add(this);
    }

    public void setAddress(Address address) {
        if (address == null) throw new NullPointerException("The address can't be null");
        this.address = address;
    }

    public Address getAddress() { return address; }

    public void setName(String name) {
        if(name == null || name.isEmpty()) throw new IllegalArgumentException("A name must be provided");
        this.name = name;
    }

    public String getName() { return name; }

    public void setNIP(long NIP) {
        if (String.valueOf(NIP).length() != 10) throw new IllegalArgumentException("NIP must have 10 digits!");
        if (extent.stream().anyMatch(c->c.NIP == NIP)) throw new IllegalArgumentException("A company with the given NIP exists");
        this.NIP = NIP;
    }

    public long getNIP() { return NIP; }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("companies.dat"));
            objectOutputStream.writeObject(Company.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("companies.dat"));
            Company.extent = (Set<Company>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Set<Company> getExtent() {
        return new HashSet<>(extent);
    }

    protected void addDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department cannot be null");
        if (extent.stream().anyMatch(c->c.departments.contains(department))) throw new IllegalArgumentException("The department is already assigned to a company, please remove it first");
        departments.add(department);
    }

    protected void removeDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department cannot be null");
        departments.remove(department);
    }

    public Set<Department> getDepartments() {
        return new HashSet<>(departments);
    }

    private void setDepartments(Collection<Department> departments) {
        if (departments != null) {
            this.departments = new HashSet<>(departments);
        }
    }

    private void setEmploymentRecords(Collection<Employment> employmentRecords) {
        if (employmentRecords != null) {
            this.employmentRecords = new HashSet<>(employmentRecords);
            for(Employment e: employmentRecords) {
                if (e.getCompany() != this) throw new IllegalArgumentException("An employment record isn't assigned to this company!");
                if (e.getEmployee().containsEmploymentRecord(e)) {
                    e.getEmployee().addEmploymentRecord(e);
                }
            }
        }
    }

    public Set<Employment> getEmploymentRecords() {
        return new HashSet<>(employmentRecords);
    }

    public void addEmploymentRecord(Employment employmentRecord) {
        if (employmentRecord == null) throw new NullPointerException("Employee cannot be null");
        if (employmentRecord.getCompany() != this) throw new IllegalArgumentException("The given employment record is assigned to a different company!");
        employmentRecords.add(employmentRecord);
        if (employmentRecord.getEmployee().containsEmploymentRecord(employmentRecord)) {
            employmentRecord.getEmployee().addEmploymentRecord(employmentRecord);
        }
    }

    public void removeEmploymentRecord(Employment employmentRecord) {
        if (employmentRecord == null) throw new NullPointerException("Employee cannot be null");
        if (employmentRecord.getCompany() != this) throw new IllegalArgumentException("The given employment record is assigned to a different company!");
        employmentRecords.remove(employmentRecord);
        if (employmentRecord.getEmployee().containsEmploymentRecord(employmentRecord)) {
            employmentRecord.getEmployee().removeEmploymentRecord(employmentRecord);
        }
    }

    public boolean containsEmploymentRecord(Employment employmentRecord) {
        return employmentRecords.contains(employmentRecord);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return NIP == company.NIP &&
                Objects.equals(name, company.name) &&
                Objects.equals(address, company.address);
    }

    @Override
    public String toString() {
        return "NIP: " + getNIP() + " Name: " + getName();
    }
}
