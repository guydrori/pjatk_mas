package pl.edu.pjwstk.s13997.mas.orm;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import pl.edu.pjwstk.s13997.mas.classes.ClassAttribute;

import java.lang.reflect.Field;
import java.util.List;

public class SessionManager {
    private static SessionManager singleton;
    private static final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
            .configure() // configures settings from hibernate.cfg.xml
            .build();
    private SessionFactory sessionFactory;

    private SessionManager() {
        try {
            sessionFactory = new MetadataSources( registry ).buildMetadata().buildSessionFactory();
            loadClassAttributes();
        }
        catch (Exception e) {
            e.printStackTrace();
            // The registry would be destroyed by the SessionFactory, but we had trouble building the SessionFactory
            // so destroy it manually.
            StandardServiceRegistryBuilder.destroy( registry );
        }
    }

    public static SessionManager getInstance() {
        if (singleton == null) {
            singleton = new SessionManager();
        }
        return singleton;
    }

    public Session openSession() {
        return sessionFactory.openSession();
    }

    public boolean isSessionOpen() {
        return true;
    }

    public void persistClassAttribute(String className, String attributeName, Object value) {
        if (!sessionFactory.isClosed()) {
            try {
                ClassAttribute attribute = new ClassAttribute(className,attributeName,value);
                Session session = openSession();
                session.beginTransaction();
                session.save(attribute);
                session.getTransaction().commit();
                session.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadClassAttributes() {
        if (!sessionFactory.isClosed()) {
            Session session = openSession();
            session = openSession();
            session.beginTransaction();
            List<ClassAttribute> classAttributes = session.createQuery("from ClassAttribute").list();
            for (ClassAttribute attribute: classAttributes) {
                try {
                    Class c = Class.forName(attribute.className);
                    Field field = c.getDeclaredField(attribute.attributeName);
                    field.setAccessible(true);
                    field.set(null,attribute.value);
                    field.setAccessible(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            session.getTransaction().commit();
            session.close();
        }
    }

    public void close() {
        sessionFactory.close();
    }
}
