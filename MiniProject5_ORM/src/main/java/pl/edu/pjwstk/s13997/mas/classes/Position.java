package pl.edu.pjwstk.s13997.mas.classes;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.StandardBasicTypes;
import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.orm.SessionManager;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name="POSITION")
public class Position implements Serializable {
    @Id
    private String name;

    public enum Permissions {READ,MODIFY,REPORTS_ONLY};
    @OneToMany
    private Set<PositionPermissions> defaultPermissions;
    @OneToMany
    private Set<Employee> employees;
    //private static Set<Employee> allAssociatedEmployees;

    public Position() {
    }

    public Position(String name,Collection<PositionPermissions> defaultPermissions,Collection<Employee> employees) {
        setName(name);
        setDefaultPermissions(defaultPermissions);
        setEmployees(employees);
    }

    public Position(String name,Collection<PositionPermissions> defaultPermissions) {
        setName(name);
        setDefaultPermissions(defaultPermissions);
        employees = new HashSet<>();
        //allAssociatedEmployees = new HashSet<>();
    }

    public Position(String name) {
        setName(name);
        defaultPermissions = new HashSet<>();
        employees = new HashSet<>();
        //allAssociatedEmployees = new HashSet<>();
    }

    public void setName(String name) {
        //if (positionList.contains(name)) throw new IllegalArgumentException("A position with the given name exists");
        this.name = name;
        //positionList.add(name);
    }

    public String getName() { return name; }

    private void setDefaultPermissions(Collection<PositionPermissions> defaultPermissions) {
        if (defaultPermissions == null) throw new NullPointerException("The Default Permissions collection cannot be null!");
        if (defaultPermissions.stream().anyMatch(pp-> pp.getPosition() != this)) throw new IllegalArgumentException("The default permission collection contains a record that doesn't belong to this position");
        this.defaultPermissions = new HashSet<>(defaultPermissions);
    }

    public void addPermissions(PositionPermissions permissions) {
        if (permissions == null) throw new NullPointerException("Permissions can't be null");
        if (permissions.getPosition() != this) throw new IllegalArgumentException("Position for the permissions object does not refer to this position");
        defaultPermissions.add(permissions);
    }

    public void removePermissions(PositionPermissions permissions) {
        if (permissions == null) throw new NullPointerException("Permissions can't be null");
        defaultPermissions.remove(permissions);
    }

    public Set<PositionPermissions> getDefaultPermissions() {
        return new HashSet<>(defaultPermissions);
    }

   public Set<PersonalDataStore> getPersonalDataStores() {
       Session session = SessionManager.getInstance().openSession();
       session.beginTransaction();
       List<PositionPermissions> positionPermissions = session.createQuery("from PositionPermissions pp where pp.position=:position")
               .setParameter("position",this).list();
       session.getTransaction().commit();
       session.close();
       return positionPermissions.stream().map(PositionPermissions::getPersonalDataStore).collect(Collectors.toSet());
    }

    public boolean hasDefaultPermissions(PersonalDataStore personalDataStore) {
        return defaultPermissions.stream().anyMatch(pp->pp.getPersonalDataStore().equals(personalDataStore));
    }

    public void removePersonalDataStore(PersonalDataStore personalDataStore) {
        if (personalDataStore == null) throw new NullPointerException("The Personal Data Store cannot be null!");
        Optional<PositionPermissions> result  = defaultPermissions.stream().filter(pp->pp.getPersonalDataStore().equals(personalDataStore)).findFirst();
        if (result.isPresent()) {
            PositionPermissions permissions = result.get();
            defaultPermissions.remove(permissions);
        }
        if (personalDataStore.hasDefaultPermissions(this)) personalDataStore.removeDefaultPermissions(this);
    }

    private void setEmployees(Collection<Employee> employees) {
        if (employees == null) throw new NullPointerException("The employees collection can't be null!");
        this.employees = new HashSet<>(employees);
       // allAssociatedEmployees = new HashSet<>(employees);
    }

    public Set<Employee> getEmployees() {
        return new HashSet<>(employees);
    }

    public void addEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee can't be null");
        if (anyContains(employee)) throw new IllegalArgumentException("The given employee is already associated to a position");
        this.employees.add(employee);
        //allAssociatedEmployees.add(employee);
    }

    public void removeEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee can't be null");
        if (anyContains(employee) && !employees.contains(employee)) throw new IllegalArgumentException("Wrong position");
        this.employees.remove(employee);
       // allAssociatedEmployees.remove(employee);
    }

    private boolean anyContains(Employee employee) {
        Session session = SessionManager.getInstance().openSession();
        session.beginTransaction();
        NativeQuery query =session.createNativeQuery("SELECT COUNT(*) AS count FROM POSITION_EMPLOYEE WHERE POSITION_EMPLOYEE.employees_PERSON_ID = :empid").addScalar("count",StandardBasicTypes.LONG);
        query.setParameter("empid",employee.getId());
        Long count = (Long)query.uniqueResult();
        session.close();
        if (count > 0) return true;
        else return false;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Objects.equals(name, position.name) &&
                Objects.equals(employees, position.employees);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, employees);
    }
}
