package pl.edu.pjwstk.s13997.mas.classes;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="CLASS_ATTRIBUTE")
public class ClassAttribute implements Serializable {
    @Id
    public String className;

    @Id
    public String attributeName;

    @Type(type="serializable")
    public Object value;

    public ClassAttribute() {

    }

    public ClassAttribute(String className, String attributeName, Object value) throws ClassNotFoundException,NoSuchFieldException {
        setClassName(className);
        setAttributeName(attributeName);
        setValue(value);
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) throws ClassNotFoundException{
        if (className == null || className.isEmpty()) throw new NullPointerException();
        if (Class.forName(className) == null) throw new IllegalArgumentException("Error finding class");
        this.className = className;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) throws ClassNotFoundException,NoSuchFieldException {
        if (attributeName == null || attributeName.isEmpty()) throw new NullPointerException();
        if (Class.forName(className).getDeclaredField(attributeName) == null) throw new IllegalArgumentException("Attribute loading error");
        this.attributeName = attributeName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
