package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="POSITION_PERMISSIONS")
public class PositionPermissions implements Serializable {
    @Id
    @Column(name="PERSONAL_DATA_STORE_ID")
    private int personalDataStoreId;
    @ManyToOne
    @PrimaryKeyJoinColumn
    private PersonalDataStore personalDataStore;

    @Id
    @Column(name="POSITON_NAME")
    private String positionName;

    @ManyToOne
    @PrimaryKeyJoinColumn
    private Position position;
    @Basic(optional = false)
    private Position.Permissions defaultPermissions;
    //private static Set<Map.Entry<PersonalDataStore,Position>> pairSet = new HashSet<>();
    //private static Set<PositionPermissions> extent = new HashSet<>();

    public PositionPermissions() {

    }

    public PositionPermissions(PersonalDataStore personalDataStore, Position position, Position.Permissions permissions) {
        setPersonalDataStore(personalDataStore);
        setPosition(position);
        setDefaultPermissions(permissions);
    }

    public PositionPermissions(PersonalDataStore personalDataStore, Position position) {
        setPersonalDataStore(personalDataStore);
        setPosition(position);
        Map.Entry<PersonalDataStore,Position> entry = new Map.Entry<>() {
            @Override
            public PersonalDataStore getKey() {
                return personalDataStore;
            }

            @Override
            public Position getValue() {
                return position;
            }

            @Override
            public Position setValue(Position value) {
                return null;
            }
        };
    }

    public void setPersonalDataStore(PersonalDataStore personalDataStore) {
        if (personalDataStore == null) throw new NullPointerException("Personal data store cannot be null!");
        this.personalDataStore = personalDataStore;
    }

    public void setPosition(Position position) {
        if (position == null) throw new NullPointerException("Position cannot be null");
        this.position = position;
    }

    public PersonalDataStore getPersonalDataStore() {
        return personalDataStore;
    }

    public Position getPosition() {
        return position;
    }

    public Position.Permissions getDefaultPermissions() {
        return defaultPermissions;
    }

    public void setDefaultPermissions(Position.Permissions permissions) {
        this.defaultPermissions = permissions;
    }

//    public static Set<PersonalDataStore> getPersonalDataStores(Position position) {
//        return pairSet.stream().filter(e->e.getValue().equals(position)).map(Map.Entry::getKey).collect(Collectors.toSet());
//    }

//    public static boolean pairExists(PersonalDataStore personalDataStore, Position position) {
//        return pairSet.contains(new Map.Entry<PersonalDataStore, Position>() {
//            @Override
//            public PersonalDataStore getKey() {
//                return personalDataStore;
//            }
//
//            @Override
//            public Position getValue() {
//                return position;
//            }
//
//            @Override
//            public Position setValue(Position value) {
//                return null;
//            }
//        });
//    }
//
//    public static void remove(PositionPermissions permissions) {
//        extent.remove(permissions);
//        pairSet.remove(new Map.Entry<PersonalDataStore, Position>() {
//            @Override
//            public PersonalDataStore getKey() {
//                return permissions.personalDataStore;
//            }
//
//            @Override
//            public Position getValue() {
//                return permissions.position;
//            }
//
//            @Override
//            public Position setValue(Position value) {
//                return null;
//            }
//        });
//    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Personal Data Store: ").append(personalDataStore.getName()).append(", Position: ").append(position.getName()).append(", Permissions: ").append(defaultPermissions.name());
        return stringBuilder.toString();
    }

}
