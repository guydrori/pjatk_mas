package pl.edu.pjwstk.s13997.mas.classes;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.classes.employee.EmployeeBuilder;
import pl.edu.pjwstk.s13997.mas.orm.SessionManager;

import java.util.List;

public class MainDemonstration {
    /*public static void main(String[] args) {
        //Example of dynamic inheritance
        Organization organization = new Organization(3627967698L,"Testowa Firma Sp. z o.o.",new Address("Marszałkowska 1","Warszawa","00-000"));
        Company company = new Company(organization,9237638925L);
        organization.changeType(new SoleProprietorship("ABC002","Ann","Smith"));
        //Example of multi inheritance
        SoleProprietorship soleProprietorship = organization.getSoleProprietorship();
        System.out.println("Organization inherited: " + soleProprietorship.getOrganization());
        System.out.println("Person inherited: " + soleProprietorship);

        //Example of overlapping inheritance
        Position position = new Position("Independent accountant");
        Person person = new Person("ABC001","John","Doe");
        Department department = new Department("Accounting","Poland");
        department.setOrganization(organization);
        Employee employee = EmployeeBuilder.build(person,organization,department,position);
        User user = new User(person,new Role("ADMIN",Role.Permissions.ADMIN),"j.doe","test"); //Example of multi-aspect inheritance
        System.out.println("\nEmployee person: " + employee.getPerson());
        System.out.println("Person user: " + person.getUser());

        //Example of polymorphism
        Entry dataStore = new PersonalDataStore("E-commerce","Admin 1:\nJohn Doe\nIT Manager\nTestowa Firma Sp. z o.o.\nj.doe@testowafirma.pl","Customer data required for realizing online orders","All e-commerce clients","Name, Address, Email","SSL Encryption, server firewall");
        Entry securityBreach = new SecurityBreach("E-commerce breach","Customer data leak","Increased server security",SecurityBreach.Status.SIGNIFICANT);
        System.out.println(dataStore.getIdentifyingDetails());
        System.out.println(securityBreach.getIdentifyingDetails());
    }*/

    public static void main(String[] args) {
        SessionManager manager = SessionManager.getInstance();
        //Example of dynamic inheritance
        Address address = new Address("Marszałkowska 1","Warszawa","00-000");
        Organization organization = new Organization(3627967698L,"Testowa Firma Sp. z o.o.",address);
        Company company = new Company(organization,9237638925L);

        Session session = manager.openSession();
        session.beginTransaction();
        session.save(address);
        session.save(organization);
        session.save(company);
        session.getTransaction().commit();
        session.close();

        //Listing elements
        session = manager.openSession();
        session.beginTransaction();
        List<Organization> organizations = session.createQuery("from Organization").list();
        for (Organization org: organizations) {
            System.out.println("Name: " + org.getName() + " Company: " + org.getCompany());
        }
        session.getTransaction().commit();
        session.close();
        //Dynamic inheritance
        organization.changeType(new SoleProprietorship("ABC002","Ann","Smith"));
        session = manager.openSession();
        session.beginTransaction();
        session.update(organization);
        session.update(company);
        session.getTransaction().commit();
        session.close();

        //Listing elements
        session = manager.openSession();
        session.beginTransaction();
        organizations = session.createQuery("from Organization").list();
        for (Organization org: organizations) {
            System.out.println("Name: " + org.getName() + " Company: " + org.getCompany() + " Sole proprietorship: " + org.getSoleProprietorship());
        }
        session.getTransaction().commit();
        session.close();

        Position position = new Position("Independent accountant");
        Person person = new Person("ABC001","John","Doe");
        session = manager.openSession();
        session.beginTransaction();
        Department department = new Department("Accounting","Poland");
        session.save(department);
        session.getTransaction().commit();
        session.close();
        department.setOrganization(organization);
        session=manager.openSession();
        session.beginTransaction();
        session.update(department);
        session.save(position);
        session.save(person);
        session.getTransaction().commit();
        Employee employee = EmployeeBuilder.build(person,organization,department,position);
        session.beginTransaction();
        session.save(employee);
        session.getTransaction().commit();
        session.close();

        //Listing elements
        session = manager.openSession();
        session.beginTransaction();
        List<Employee> employees = session.createQuery("from Employee").list();
        for (Employee emp: employees) {
            System.out.println("Name: " + emp.getFulllName());
        }
        session.getTransaction().commit();
        session.close();

        Employee.setMinSalary(5000);

        manager.close();
    }
}
