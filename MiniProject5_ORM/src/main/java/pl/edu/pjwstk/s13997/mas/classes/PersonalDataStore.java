package pl.edu.pjwstk.s13997.mas.classes;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@Table(name="PERSONAL_DATA_STORE")
public class PersonalDataStore extends Entry implements Serializable {
    @Basic(optional = false)
    private String name;
    private String location; //Optional attribute
    @Basic(optional = false)
    @Column(length=1024)
    private String adminDetails;
    @Basic(optional = false)
    @Type(type = "text")
    @Column(length=10000)
    private String processingObjectives;
    @Basic(optional = false)
    @Type(type = "text")
    @Column(length=10000)
    private String targetPeopleCategoryDescription;
    @Basic(optional = false)
    @Type(type = "text")
    @Column(length=10000)
    private String personalDataCategory;
    @Basic(optional = false)
    @Type(type = "text")
    @Column(length=10000)
    private String dataDisclosureRecipientCategoryDescription;
    @Basic(optional = false)
    @Type(type = "text")
    @Column(length=10000)
    private String plannedDeletionDates;
    @Basic(optional = false)
    @Type(type = "text")
    @Column(length=10000)
    private String securityResourcesDescription;
    @ElementCollection
    private List<String> dataDisclosureRecipients; //Multi-value attribute
    @OneToMany
    private Set<PositionPermissions> defaultPermissions;
    //private static Set<PersonalDataStore> extent = new HashSet<>();

    public PersonalDataStore(int id, String name, String location, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String dataDisclosureRecipientCategoryDescription, String plannedDeletionDates, String securityResourcesDescription, Collection<String> dataDisclosureRecipients, Collection<PositionPermissions> defaultPermissions) {
        super(id);
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        if (dataDisclosureRecipients != null) this.dataDisclosureRecipients = new ArrayList<>(dataDisclosureRecipients);
        setDefaultPermissions(defaultPermissions);
        //extent.add(this);
    }

    public PersonalDataStore(int id, String name, String location, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String dataDisclosureRecipientCategoryDescription, String plannedDeletionDates, String securityResourcesDescription, Collection<String> dataDisclosureRecipients) {
        super(id);
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        if (dataDisclosureRecipients != null) this.dataDisclosureRecipients = new ArrayList<>(dataDisclosureRecipients);
        defaultPermissions = new HashSet<>();
        //extent.add(this);
    }

    public PersonalDataStore(String name, String location, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String dataDisclosureRecipientCategoryDescription, String plannedDeletionDates, String securityResourcesDescription) {
        super();
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        defaultPermissions = new HashSet<>();
        //extent.add(this);
    }

    public PersonalDataStore(String name, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String securityResourcesDescription) {
        super();
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        defaultPermissions = new HashSet<>();
        //extent.add(this);
    }

    //Optional attribute
    public void setLocation(String location) {
        if(location == null || location.isEmpty()) this.location = "N/A";
        else this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setAdminDetails(String adminDetails) {
        if(adminDetails == null || adminDetails.isEmpty()) throw new IllegalArgumentException("Admin details must be provided");
        this.adminDetails = adminDetails;
    }

    public String getAdminDetails() {
        return adminDetails;
    }

    public void setProcessingObjectives(String processingObjectives) {
        if(processingObjectives == null || processingObjectives.isEmpty()) throw new IllegalArgumentException("Processing objectives must be provided");
        this.processingObjectives = processingObjectives;
    }

    public String getProcessingObjectives() {
        return processingObjectives;
    }

    public void setTargetPeopleCategoryDescription(String targetPeopleCategoryDescription) {
        if(targetPeopleCategoryDescription == null || targetPeopleCategoryDescription.isEmpty()) throw new IllegalArgumentException("The category of target people must be described");
        this.targetPeopleCategoryDescription = targetPeopleCategoryDescription;
    }

    public String getTargetPeopleCategoryDescription() {
        return targetPeopleCategoryDescription;
    }

    public void setPersonalDataCategory(String personalDataCategory) {
        if(personalDataCategory == null || personalDataCategory.isEmpty()) throw new IllegalArgumentException("The personal data category must be provided");
        this.personalDataCategory = personalDataCategory;
    }

    public String getPersonalDataCategory() {
        return personalDataCategory;
    }

    public void setDataDisclosureRecipientCategoryDescription(String dataDisclosureRecipientCategoryDescription) {
        if(dataDisclosureRecipientCategoryDescription == null || dataDisclosureRecipientCategoryDescription.isEmpty()) this.dataDisclosureRecipientCategoryDescription = "N/A";
        else this.dataDisclosureRecipientCategoryDescription = dataDisclosureRecipientCategoryDescription;
    }

    public String getDataDisclosureRecipientCategoryDescription() {
        return dataDisclosureRecipientCategoryDescription;
    }

    public void setPlannedDeletionDates(String plannedDeletionDates) {
        if(plannedDeletionDates == null || plannedDeletionDates.isEmpty()) this.plannedDeletionDates = "N/A";
        else this.plannedDeletionDates = plannedDeletionDates;
    }

    public String getPlannedDeletionDates() {
        return plannedDeletionDates;
    }

    public void setSecurityResourcesDescription(String securityResourcesDescription) {
        if(securityResourcesDescription == null || securityResourcesDescription.isEmpty()) throw new IllegalArgumentException("Security resources must be described");
        this.securityResourcesDescription = securityResourcesDescription;
    }

    public String getSecurityResourcesDescription() {
        return securityResourcesDescription;
    }

    public void addDataDisclosureRecipient(String recipient) { dataDisclosureRecipients.add(recipient);}

    public boolean removeDataDisclosureRecipient(String recipient) { return dataDisclosureRecipients.remove(recipient); }

    public List<String> getDataDisclosureRecipients() {
        return new LinkedList<>(dataDisclosureRecipients);
    }

    private void setDefaultPermissions(Collection<PositionPermissions> defaultPermissions) {
        if (defaultPermissions.stream().anyMatch(pp->pp.getPersonalDataStore() != this)) throw new IllegalArgumentException("The permission collection contains a record that refers to another data store");
        this.defaultPermissions = new HashSet<>(defaultPermissions);
    }

    public void addDefaultPermissions(PositionPermissions permissions) {
        if (permissions == null) throw new NullPointerException("Permissions can't be null");
        if (permissions.getPersonalDataStore() != this) throw new IllegalArgumentException("Permissions can't refer to another data store");
        defaultPermissions.add(permissions);
        if (!permissions.getPosition().hasDefaultPermissions(this)) permissions.getPosition().addPermissions(permissions);
    }

    public void removeDefaultPermissions(Position position) {
        if (position == null) throw new NullPointerException("Position can't be null");
        Optional<PositionPermissions> result = defaultPermissions.stream().filter(pp->pp.getPosition() == position).findFirst();
        if (result.isPresent()) {
            defaultPermissions.remove(result.get());
            position.removePersonalDataStore(this);
        }
    }

    public Set<PositionPermissions> getDefaultPermissionSet() {
        return new HashSet<>(defaultPermissions);
    }

    public Set<Position> getPositionSet() {
        return defaultPermissions.stream().map(PositionPermissions::getPosition).collect(Collectors.toSet());
    }

    public PositionPermissions getDefaultPermissions(Position position) {
        if (position == null) throw new NullPointerException("Position can't be null");
        if (!defaultPermissions.stream().anyMatch(pp->pp.getPosition() == position)) throw new IllegalArgumentException("No default permissions are defined for the given position");
        return defaultPermissions.stream().filter(pp->pp.getPosition() == position).findFirst().get();
    }

    public boolean hasDefaultPermissions(Position position) {
        return defaultPermissions.stream().anyMatch(pp->pp.getPosition() == position);
    }

    public void setName(String name) {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("Name must be provided!");
        this.name = name;
    }

    public String getName() { return name; }

    @Override
    public void print() {
        System.out.println(this);
    }

    @Override
    public String getIdentifyingDetails() {
        return "Personal Data Store: \"" + this.getName() + "\" Location: " + this.getLocation();
    }

//    public static void saveExtent() {
//        try {
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("dataStores.dat"));
//            objectOutputStream.writeObject(PersonalDataStore.extent);
//            objectOutputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void loadExtent() {
//        try {
//            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("dataStores.dat"));
//            PersonalDataStore.extent = (Set<PersonalDataStore>) objectInputStream.readObject();
//            objectInputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonalDataStore dataStore = (PersonalDataStore) o;
        return getId() == dataStore.getId() &&
                Objects.equals(name, dataStore.name) &&
                Objects.equals(location, dataStore.location) &&
                Objects.equals(adminDetails, dataStore.adminDetails) &&
                Objects.equals(processingObjectives, dataStore.processingObjectives) &&
                Objects.equals(targetPeopleCategoryDescription, dataStore.targetPeopleCategoryDescription) &&
                Objects.equals(personalDataCategory, dataStore.personalDataCategory) &&
                Objects.equals(dataDisclosureRecipientCategoryDescription, dataStore.dataDisclosureRecipientCategoryDescription) &&
                Objects.equals(plannedDeletionDates, dataStore.plannedDeletionDates) &&
                Objects.equals(securityResourcesDescription, dataStore.securityResourcesDescription) &&
                Objects.equals(dataDisclosureRecipients, dataStore.dataDisclosureRecipients);
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), name, location, adminDetails, processingObjectives, targetPeopleCategoryDescription, personalDataCategory, dataDisclosureRecipientCategoryDescription, plannedDeletionDates, securityResourcesDescription, dataDisclosureRecipients);
    }
}
