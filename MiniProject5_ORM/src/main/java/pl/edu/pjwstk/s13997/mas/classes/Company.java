package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.interfaces.IOrganization;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
@Entity
@Table(name="COMPANY")
public class Company implements IOrganization {
    @OneToOne(targetEntity = Organization.class,optional = false)
    private Organization whole;
    @Id
    @Column(name="KRS",length = 10)
    private long KRS;

    public Company(Organization whole, long krs) {
        setOrganization(whole);
        setKRS(krs);
    }

    public Company(long krs) {
        setKRS(krs);
    }

    public Company() {
    }

    public long getKRS() {
        return KRS;
    }

    public void setKRS(long KRS) {
        if (String.valueOf(KRS).length() != 10) throw new IllegalArgumentException("KRS number must have 10 digits!");
        //if (extent.stream().anyMatch(c->c.KRS == KRS)) throw new IllegalArgumentException("A company with the given KRS number exists");
        this.KRS = KRS;
    }

    public IOrganization getOrganiztaion() {
        return whole;
    }

    public void setOrganization(Organization whole) {
        if (whole == null) {
            if (this.whole != null) {
                this.whole.removeCompany(this);
            }
        } else {
            if (this.whole != whole) {
                if (this.whole != null) throw new IllegalArgumentException("This employee already belongs to a Person!");
                if (whole.getCompany() != null && whole.getCompany() != this) throw new IllegalArgumentException("An employee object is already assigned to this Person");
                this.whole = whole;
                if (whole.getCompany() != this) {
                    whole.setCompany(this);
                }
            }
        }
    }

    @Override
    public void setAddress(Address address) {
        if (whole != null) whole.setAddress(address);
        else throw new NullPointerException();
    }

    @Override
    public Address getAddress() {
        if (whole != null) return whole.getAddress();
        else throw new NullPointerException();
    }

    @Override
    public void setName(String name) {
        if (whole != null) whole.setName(name);
        else throw new NullPointerException();
    }

    @Override
    public String getName() {
        if (whole != null) return whole.getName();
        else throw new NullPointerException();
    }

    @Override
    public void setNIP(long NIP) {
        if (whole != null) whole.setNIP(NIP);
        else throw new NullPointerException();
    }

    @Override
    public long getNIP() {
        if (whole != null) return whole.getNIP();
        else throw new NullPointerException();
    }

    @Override
    public Set<Department> getDepartments() {
        if (whole != null) return whole.getDepartments();
        else throw new NullPointerException();
    }

    @Override
    public String toString() {
        return String.valueOf(getKRS());
    }
}
