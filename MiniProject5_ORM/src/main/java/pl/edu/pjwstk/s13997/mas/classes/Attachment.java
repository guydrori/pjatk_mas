package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.*;


import java.util.HashSet;
import java.util.Set;
@Entity
@Table( name="ATTACHMENT")

public class Attachment {
    @Id
    @Column(name="FILE_NAME")
    private String fileName;

    @Column(name="URL",unique = true)
    private String URL;
    //private static Set<Attachment> extent = new HashSet<>();

    public Attachment(String fileName, String URL) {
        setFileName(fileName);
        setURL(URL);
        //extent.add(this);
    }

    public Attachment() {
    }

    public void setFileName(String fileName) {
        if(fileName == null || fileName.isEmpty()) throw new IllegalArgumentException("A file name must be provided");
//        if (extent.stream().anyMatch(u->u.fileName.equals(fileName))) throw new IllegalArgumentException("An attachment exists with the given filename");
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setURL(String URL) {
        if(URL == null || URL.isEmpty()) throw new IllegalArgumentException("A URL must be provided");
//        if (extent.stream().anyMatch(u->u.URL.equals(URL))) throw new IllegalArgumentException("An attachment exists with the given URL");
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }
}
