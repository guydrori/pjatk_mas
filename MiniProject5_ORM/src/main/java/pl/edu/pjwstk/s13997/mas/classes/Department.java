package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import javax.persistence.*;
import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="DEPARTMENT")
public class Department implements Serializable {
    @Id
    @GeneratedValue
    @Column(name="ID")
    private int id;
    @Basic(optional = false)
    @Column(name="NAME",unique = true)
    private String name;
    @Basic(optional = false)
    @Column(name="REGION")
    private String region;
    @ManyToOne
    private Organization organization;
    @OneToMany
    private Set<Employee> employees;
   // private static Set<Department> extent = new HashSet<>();

    public Department(String name, String region, Organization organization, Collection<Employee> employees) {
        setName(name);
        setRegion(region);
        setOrganization(organization);
        setEmployees(employees);
        //extent.add(this);
    }

    public Department(String name,String region,Organization organization) {
        setName(name);
        setRegion(region);
        setOrganization(organization);
        employees = new HashSet<>();
       // extent.add(this);
    }

    public Department(String name,String region) {
        setName(name);
        setRegion(region);
        employees = new HashSet<>();
    }

    public Department() {
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        if(name == null || name.isEmpty()) throw new IllegalArgumentException("A name must be provided");
        //if (extent.stream().anyMatch(d->d.name.equals(name))) throw new IllegalArgumentException("A department with the given name exists");
        this.name = name;
    }

    public String getName() { return name; }

    public void setRegion(String region) {
        if(region == null || region.isEmpty()) throw new IllegalArgumentException("A region must be provided");
        this.region = region;
    }

    public String getRegion() { return region; }

    public void setOrganization(Organization organization) {
        if (organization == null) throw new NullPointerException("Organization must not be null!");
        if (this.organization != null) {
            this.organization.removeDepartment(this);
        }
        this.organization = organization;
        organization.addDepartment(this);
    }

    public Organization getOrganization() {
        return organization;
    }

    private void setEmployees(Collection<Employee> employees) {
        if (employees == null) throw new NullPointerException();
        this.employees = new HashSet<>(employees);
    }

    public void addEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee cannot be null!");
        //if (anyContains(employee.getPerson())) throw new IllegalArgumentException("An employee with the given ID already works in a department");
        employees.add(employee);
        employee.setDepartment(this);
    }

    public void removeEmployee(Employee employee,Department newDepartment) {
        if (employee == null) throw new NullPointerException("Employee cannot be null!");
        if (newDepartment == null) throw new NullPointerException("New department can't be null!");
        employees.remove(employee);
        newDepartment.addEmployee(employee);
    }

    public void removeEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee cannot be null!");
        if (employee.getPerson() != null) throw new NullPointerException("Employee that's part of a Person must belong to a department!");
        employees.remove(employee);
    }

    public Set<Employee> getEmployees() {
        return new HashSet<>(employees);
    }

    public boolean contains(Employee employee) {
        return employees.contains(employee);
    }

//    public static boolean anyContains(Person person) {
//        return extent.stream().anyMatch(d->d.employees.stream().anyMatch(e->{
//           if (e.getPerson() != null) return e.getPerson().equals(person);
//           else return false;
//        }));
//    }

//    public static void saveExtent() {
//        try {
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("depts.dat"));
//            objectOutputStream.writeObject(Department.extent);
//            objectOutputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();;
//        }
//    }
//
//    public static void loadExtent() {
//        try {
//            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("depts.dat"));
//            Department.extent = (Set<Department>)objectInputStream.readObject();
//            objectInputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();;
//        }
//    }

    @Override
    public String toString() {
        return "Organization: " + organization.getName() + ", Name: " + getName();
    }
}
