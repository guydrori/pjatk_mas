package pl.edu.pjwstk.s13997.mas.classes.interfaces;

import pl.edu.pjwstk.s13997.mas.classes.Address;
import pl.edu.pjwstk.s13997.mas.classes.Department;

import java.util.Set;

public interface IOrganization {

    public void setAddress(Address address);

    public Address getAddress();

    public void setName(String name);

    public String getName();

    public void setNIP(long NIP);

    public long getNIP();

    public Set<Department> getDepartments();


}
