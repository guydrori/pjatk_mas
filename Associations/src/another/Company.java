package another;

import java.util.HashSet;
import java.util.Set;

public class Company {
	private Set<Employment> employment = new HashSet<>();

	public Set<Employment> getEmployment() {
		return employment;
	}

	public void addEmployment(Employment employment) {
		if (employment.getCompany() != this) {
			throw new IllegalArgumentException("Employment company is different.");
		}
		
		if (!this.employment.contains(employment))
		{
			this.employment.add(employment);
		}
	}
}
