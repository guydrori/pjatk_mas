package another;

import java.util.*;

public class Employment {
	private Date hireDate;
	private Company company;
	private Employee employee;

	public Employment(Date hireDate, Company company, Employee employee) {
		super();

		setHireDate(hireDate);
		setCompany(company);
		setEmployee(employee);
	}
	
	public Date getHireDate() {
		return hireDate;
	}

	public void setHireDate(Date hireDate) {
		if (hireDate == null)
		{
			throw new IllegalArgumentException("hireDate is null.");
		}
		else
		{
			this.hireDate = hireDate;
		}
	}

	public Company getCompany() {
		return company;
	}

	private void setCompany(Company company) {
		if (company == null)
			throw new IllegalArgumentException("Company is null.");
		else
		{
			this.company = company;
			this.company.addEmployment(this);
		}
	}

	public Employee getEmployee() {
		return employee;
	}

	private void setEmployee(Employee employee) {
		if (employee == null)
			throw new IllegalArgumentException("Employee is null.");
		else
		{
			this.employee = employee;
			this.employee.setEmployment(this);
		}
	}
}
