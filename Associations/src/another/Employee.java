package another;

public class Employee {
	private Employment employment;

	public Employment getEmployment() {
		return employment;
	}

	public void setEmployment(Employment employment) {
		if (employment != null) {
			throw new IllegalArgumentException("Employment is null.");
		}
		
		if (employment.getEmployee() != this) {
			throw new IllegalArgumentException("Employment employee is different.");
		}
		else
		{
			this.employment = employment;
		}
	}
}
