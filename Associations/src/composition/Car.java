package composition;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Car {
	private Set<Part> parts = new HashSet<Car.Part>();
	
	public void createPart(String name){
		parts.add(new Part(name));
	}
	
	public void removePart(String name){
		if(name != null){
			for(Part part : parts){
				if(part.getName().equals(name)){
					parts.remove(part);
				}
			}
		}else{
			throw new IllegalArgumentException("Name should not be null");
		}
	}
	
	public List<String> getPartNames(){
		List<String> partNames = new ArrayList<String>();
		for(Part part : parts){
			partNames.add(part.getName());
		}
		return partNames;
	}
	
	private class Part{
		private String name;
		
		public Part(String name) {
			setName(name);
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			if(name != null){
				this.name = name;
			}else{
				throw new IllegalArgumentException("Name could not be null");
			}
		}
	}
}
