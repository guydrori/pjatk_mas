package qualified;

import java.util.HashMap;
import java.util.Map;

public class Company {
	
	private Map<String,Employee> employees = new HashMap<String,Employee>();
	
	
	public Employee getEmployee(String id){
		if(id != null){
			return employees.get(id);
		}else{
			throw new RuntimeException("Null given key");
		}
	}
	
	
	public void addEmployee(Employee newEmplo){
		if(newEmplo == null){
			throw new RuntimeException("Null Employee given");
		}else{
			if(!employees.containsKey(newEmplo.getId())){
				employees.put(newEmplo.getId(), newEmplo);
				newEmplo.setCompany(this);
			}else{
				throw new RuntimeException("Already in that company");
			}
		}
	}
	
	public void removeEmployee(String id){
		if(id == null){
			throw new RuntimeException("Null id given");
		}else{
			employees.remove(id);
		}
	}

}
