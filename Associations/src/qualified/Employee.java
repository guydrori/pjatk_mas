package qualified;

public class Employee {
	
	private String id;
	private Company company;
	

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		if(company == null){
			throw new RuntimeException("Null company given");
		}else{
			this.company = company;
			company.addEmployee(this);
		}
	}
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		if(id == null){
			throw new RuntimeException("Null id given");
		}else{
			this.id = id;
		}
	}
	
	

}
