package first;
import java.util.ArrayList;


public class Company {
	
	public Company(Employee firstEmployee) {
		addEmployee(firstEmployee);
	}
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	private ArrayList<Employee> employees = new ArrayList<Employee>();
	
	public ArrayList<Employee> getEmployees() {
		return new ArrayList<Employee>(employees);
	}
	
	public void addEmployee(Employee emp){
		if(emp != null){
			if(!employees.contains(emp)){
				employees.add(emp);
				emp.setCompany(this);
			}
		}
	}
	
	public void removeEmployee(Employee emp) {
		if (employees.contains(emp)) {
			employees.remove(emp);
			emp.setCompany(null);
		}
	}
}
