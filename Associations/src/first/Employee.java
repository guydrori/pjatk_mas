package first;
import java.util.HashSet;
import java.util.Set;


public class Employee {
	private String firstName;
	
	private Car car;
	private Company company;
	private Set<Project> projects = new HashSet<>();

	public Employee(String firstName) {
		super();
		this.firstName = firstName;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		
		if(this.car != car){
			if(this.car != null) {
				this.car.setOwner(null);
			}
			this.car = car;
			car.setOwner(this);
		}
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void addProject(Project p) {
		if(p == null) {
			throw new RuntimeException("project musn't be null");
		} else {
			if (!projects.contains(p)) {
				projects.add(p);
				p.addEmployee(this);
			}
		}
	}

	@Override
	public String toString() {
		return "Employee [firstName=" + firstName + ", car=" + car.getMake() + "]";
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		if(this.company != company){
			if (this.company != null) {
				this.company.removeEmployee(this);
			}
			this.company = company;
			this.company.addEmployee(this);
		}
	}
	
	
}
