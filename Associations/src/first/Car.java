package first;

public class Car {
	private String make;
	
	private Employee owner;

	public Car(String make, Employee owner) {
		super();
		this.make = make;
		this.setOwner(owner);
	}

	public Employee getOwner() {
		return owner;
	}

	public void setOwner(Employee owner) {
//		if(owner != null) {
			if(this.owner != owner){
				this.owner = owner;
				if(owner != null) {
					owner.setCar(this);
				}
			}
//		}else{
//			throw new RuntimeException("Owner must be not null");
//		}
			
	}
	
	public String getMake() {
		return make;
	}
	public void setMake(String make) {
		this.make = make;
	}

	@Override
	public String toString() {
		return "Car [make=" + make + ", owner=" + (owner != null ? owner.getFirstName() : "") + "]";
	}
	
	

}
