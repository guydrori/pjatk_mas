package first;
import java.util.ArrayList;

public class Project {	
	private String name;
	private ArrayList<Employee> employees = new ArrayList<>();
	
	public Project(Employee emp) {
		addEmployee(emp);
	}
	
	public void addEmployee(Employee emp) {
		if(emp == null) {
			throw new RuntimeException("emp should not be null");
		} else if(employees.size() >= 20) {
			throw new RuntimeException("Maximum number of employees reached.");
		} else {
			if(!employees.contains(emp)) {
				employees.add(emp);
				emp.addProject(this);
			}
		} 
	}
	
	public void removeEmployee(Employee emp) {
		if (emp == null) {
			throw new RuntimeException("employee musn't be null");
		} else if (employees.size() <= 1) {
			throw new RuntimeException("Cannot remove the first employee");
		} else {
			employees.remove(emp);
		}
	}
	
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
