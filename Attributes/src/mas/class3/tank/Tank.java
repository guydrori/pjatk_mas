package mas.class3.tank;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Tank {
	private static List<Tank> extent = new ArrayList<>();
	private String name;
	private TankClass tankClass;
	private String nation;
	private String turret;
	private Integer turretTraverseSpeed;
	private String gunName;
	private int gunPenetration;
	private int gunDamage;
	private int armor;
	private Integer maxSpeed;
	private int maxDamage;
	private int damage;

	public Tank(String name, TankClass tankClass, String nation,
			String gunName, int gunPenetration, int gunDamage, int armor,
			int maxDamage, int damage) {
		this();
		setName(name);
		setTankClass(tankClass);
		setNation(nation);
		setGunName(gunName);
		setGunPenetration(gunPenetration);
		setGunDamage(gunDamage);
		setArmor(armor);
		setMaxDamage(maxDamage);
		setDamage(damage);
//		this.name = name;
//		this.tankClass = tankClass;
//		this.nation = nation;
//		this.gunName = gunName;
//		this.gunPenetration = gunPenetration;
//		this.gunDamage = gunDamage;
//		this.armor = armor;
//		this.maxDamage = maxDamage;
//		this.damage = damage;
	}

	private Tank() {
		super();
		Tank.extent.add(this);
	}
	
	public static List<Tank> getTanks(){
		List<Tank> tmp = new ArrayList<>(Tank.extent);
		return tmp;
	}
	public static List<Tank> getTanks(Integer minRemainDamage){
		List<Tank> tmp = new ArrayList<>(Tank.extent);
		if(minRemainDamage==null){
			return tmp;
		}else{
			List<Tank> results = new ArrayList<>();
			for(Tank t: tmp){
				if(t.getRemainDamage()<minRemainDamage){
					results.add(t);
				}
			}
			return results;
		}
	}
	
	public static List<Tank> getDestroyedTanks() {
		List<Tank> tmp = new ArrayList<>(Tank.extent);
		
		Iterator<Tank> iterator = tmp.iterator();
		for (;iterator.hasNext();) {
			Tank t = iterator.next();
			if (t.getRemainDamage() > 0)
				iterator.remove();
		}
		
		return tmp;
	}

	public boolean takeDamage(Tank tank) {
		if (tank.getGunPenetration() > armor) {
			this.setDamage(damage + tank.getGunDamage());
		}
		return damage > maxDamage;
	}

	public void shoot(Tank t) {
		t.takeDamage(this);
	}

	public Integer getRemainDamage() {
		return getMaxDamage() - getDamage();
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		if (damage < 0) throw new IllegalArgumentException("Damage cannot be negative!");
		this.damage = damage;
	}

	public int getMaxDamage() {
		return maxDamage;
	}

	public void setMaxDamage(int maxDamage) {
		if (maxDamage < 0) throw new IllegalArgumentException("Max Damage cannot be negative!");
		if (maxDamage < damage) throw new IllegalArgumentException("Max Damage cannot be smaller than the current damage!");
		this.maxDamage = maxDamage;
	}

	public Integer getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(Integer maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		if (armor < 0) throw new IllegalArgumentException("Armor cannot be negative!");
		this.armor = armor;
	}

	public int getGunDamage() {
		return gunDamage;
	}

	public void setGunDamage(int gunDamage) {
		if (gunDamage < 0) throw new IllegalArgumentException("Gun Damage cannot be negative!");
		this.gunDamage = gunDamage;
	}

	public Integer getTurrtTraverseSpeed() {
		return turretTraverseSpeed;
	}

	public void setTurrtTraverseSpeed(Integer turrtTraverseSpeed) {
		if (turret == null || turret.isEmpty()) throw new RuntimeException("No turret exists!");
		this.turretTraverseSpeed = turrtTraverseSpeed;
	}

	public String getTurret() {
		return turret;
	}

	public void setTurret(String turret) { this.turret = turret; }

	public TankClass getTankClass() {
		return tankClass;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		if (nation == null) {
			throw new IllegalArgumentException("Nation is null");
		} else if (nation.isEmpty()) {
			throw new IllegalArgumentException("Nation is empty");
		} else
			this.nation = nation;
	}

	public void setTankClass(TankClass tankClass) {
		if (tankClass == null) {
			throw new IllegalArgumentException("TankClass is null");
		} else {
			this.tankClass = tankClass;
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Name is null");
		} else if (name.isEmpty()) {
			throw new IllegalArgumentException("Name is empty");
		} else {
			this.name = name;
		}
	}

	public String getGunName() {
		return gunName;
	}

	public void setGunName(String gunName) {
		if (gunName == null) {
			throw new IllegalArgumentException("gunName is null");
		}else if (gunName.isEmpty()) {
			throw new IllegalArgumentException("gunName is empty");
		} else {
			this.gunName = gunName;
		}
	}

	public int getGunPenetration() {
		return gunPenetration;
	}

	public void setGunPenetration(int gunPenetration) {
		if (gunPenetration < 0) throw new IllegalArgumentException("Gun Penetration cannot be negative!");
		this.gunPenetration = gunPenetration;
	}
}
