package mas.class3.movie;

public enum MovieRating {
	AL("For all"),
	_7("For 7+"),
	_12("For 12+"),
	_15("For 15+"),
	_18("For 18+"),
	_21("For 21+");
	public final String description;
	private MovieRating(String desc){
		description = desc;
	}
}
