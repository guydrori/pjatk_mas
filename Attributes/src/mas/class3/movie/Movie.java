package mas.class3.movie;

import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Movie implements Serializable {
	private long id;
	private String title;
	private Date releasedDate;
	private Set<String> category = new HashSet<>();
	private MovieRating rating;
	@Nullable
	//Non-primitive types should be used for optional attributes
	private Integer budget;
	@Nullable
	private Integer income;

	private static Set<Movie> extent = new HashSet<>();

	public Movie(int id, String title, Date releasedDate, Set<String> categories, MovieRating rating) {
		super();
		//For required parameters it is a good idea to use setters to verify conditions and contents
		setId(id);
		setTitle(title);
		setCategory(categories);
		setReleasedDate(releasedDate);
		setRating(rating);
		Movie.extent.add(this);
	}

	private static Set<String> knownCategories = new HashSet<>();
	static {
		knownCategories.add("SF");
		knownCategories = Collections.unmodifiableSet(knownCategories);
	}
	
	public static Set<String> getKnownCategories() {
		return new HashSet<>(knownCategories);
	}
	
	public static void addKnownCategory(String cat) {
		if(cat == null) {
			throw new RuntimeException("category should not be null");
		}
		knownCategories.add(cat);
	}
	
	public MovieRating getRating() {
		return rating;
	}

	public void setRating(MovieRating rating) {
		if(rating != null)this.rating = rating;
		else throw new RuntimeException("Invalid value - rating is null");
	}

	public void setCategory(Set<String> category) {
		if(category != null && !category.isEmpty()) {
			this.category = category;			
		} else {
			throw new IllegalArgumentException();
		}
		
	}

	public boolean addCategory(String e) {
		return category.add(e);
	}

	public boolean removeCategory(String s) {
		if(category.size() < 2) {
			throw new RuntimeException("movie should have at least 1 category");
		}
		return category.remove(s);
	}

	//To avoid external modification of the collection in a way that hinders integrity, we return a copy
	public Set<String> getCategory() {
		return new HashSet<String>(category);
	}

	public Date getReleasedDate() {
		return releasedDate;
	}

	public void setReleasedDate(Date releasedDate) {
		if (releasedDate == null) {
			throw new IllegalArgumentException("releasedDate cant be null");
		} else {
			this.releasedDate = releasedDate;
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		if (title == null) {
			throw new IllegalArgumentException("Title cant be null");
		} else {
			this.title = title;
		}
	}

	//Serialization is an example of persistence
	public static void saveExtent() {
		try ( ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data.dat")); ) {
			oos.writeObject(Movie.extent);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void loadExtent() {
		try ( ObjectInputStream oos = new ObjectInputStream(new FileInputStream("data.dat")); ) {
			Movie.extent = (Set<Movie>) oos.readObject();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public Integer getProfit() {
		if (budget != null && income != null) {
			return income - budget;
		} else {
			return null;
		}
	}

	public static List<Movie> findByCategory(String category) {
		return Movie.extent.stream()
				.filter(movie-> movie.category.contains(category))
				.collect(Collectors.toList());
	}

	public static List<Movie> findByRating(MovieRating rating) {
		return Movie.extent.stream()
				.filter(movie-> movie.rating.equals(rating))
				.collect(Collectors.toList());
	}

}
