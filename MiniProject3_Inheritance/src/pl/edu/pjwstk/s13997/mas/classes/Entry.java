package pl.edu.pjwstk.s13997.mas.classes;

import java.time.LocalDate;
import java.time.LocalDateTime;

public abstract class Entry {
    private static int idCounter = 1;
    private int id;
    private LocalDateTime creationTime;

    public Entry() {
        id = idCounter;
        idCounter++;
        creationTime = LocalDateTime.now();
    }

    public Entry(int id) {
        setId(id);
        creationTime = LocalDateTime.now();
    }

    public Entry(int id,LocalDateTime creationTime) {
        setCreationTime(creationTime);
        setId(id);
    }

    public Entry(LocalDateTime creationTime) {
        setCreationTime(creationTime);
        id = idCounter;
        idCounter++;
    }

    private void setId(int id) {
        if (id >= idCounter) {
            this.id = id;
            if (id == idCounter) idCounter++;
            else idCounter = id + 1;
        } else {
            throw new IllegalArgumentException("The given ID has been already assigned or it is invalid");
        }
    }

    public int getId() {
        return id;
    }

    private void setCreationTime(LocalDateTime creationTime) {
        if (creationTime == null) throw new NullPointerException("Creation time must not be null");
        this.creationTime = creationTime;
    }

    public LocalDateTime getCreationTime() { return creationTime; }

    public abstract void print();

    public abstract String getIdentifyingDetails();

}
