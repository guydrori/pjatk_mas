package pl.edu.pjwstk.s13997.mas.classes.employee;

import pl.edu.pjwstk.s13997.mas.classes.Organization;
import pl.edu.pjwstk.s13997.mas.classes.Department;
import pl.edu.pjwstk.s13997.mas.classes.Person;
import pl.edu.pjwstk.s13997.mas.classes.Position;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Map;

public class EmployeeBuilder {
    public static Employee build(Person whole, Map<Long,Organization> companies, Department department, Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(whole)) throw new IllegalArgumentException("An employee object with the given Person already exists in a department");
        Employee newEmployee = new Employee(whole,companies,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }

    public static Employee build(Person whole, Organization organization, Department department, Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(whole)) throw new IllegalArgumentException("An employee object with the given Person already exists in a department");
        Employee newEmployee = new Employee(whole, organization,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }

    public static Employee build(Person whole, Collection<LocalDate> trainingDates, Organization organization, Department department, Position position) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (Department.anyContains(whole)) throw new IllegalArgumentException("An employee object with the given Person already exists in a department");
        Employee newEmployee = new Employee(whole,trainingDates, organization,department,position);
        department.addEmployee(newEmployee);
        return newEmployee;
    }
}
