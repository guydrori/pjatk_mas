package pl.edu.pjwstk.s13997.mas.classes.employee;

import pl.edu.pjwstk.s13997.mas.classes.Organization;
import pl.edu.pjwstk.s13997.mas.classes.Department;
import pl.edu.pjwstk.s13997.mas.classes.Person;
import pl.edu.pjwstk.s13997.mas.classes.Position;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class Employee implements Serializable {
    private Person whole;
    private HashMap<Long,Organization> companies;
    private Department department;
    private List<LocalDate> trainingDates; //Multi-value attribute
    private Position position;
    private static Set<Employee> extent = new HashSet<>();

    Employee(Person whole, Map<Long,Organization> companies, Department department, Position position) {
        setPerson(whole);
        trainingDates = new LinkedList<>();
        setCompanies(companies);
        this.department = department;
        setPosition(position);
        extent.add(this);
    }

    Employee(Person whole, Organization organization, Department department, Position position) {
        setPerson(whole);
        trainingDates = new LinkedList<>();
        setCompany(organization);
        this.department = department;
        setPosition(position);
        extent.add(this);
    }

    Employee(Person whole, Collection<LocalDate> trainingDates, Organization organization, Department department, Position position) {
        setPerson(whole);
        if (trainingDates == null) throw new NullPointerException("Training dates collection can't be null");
        this.trainingDates = new LinkedList<>(trainingDates);
        setCompany(organization);
        this.department = department;
        setPosition(position);
        extent.add(this);
    }

    //Derived attribute
    public LocalDate getMostRecentTrainingDate() {
        if (trainingDates != null) return trainingDates.stream().max((d1,d2)-> {return d1.compareTo(d2);}).get();
        else return null;
    }

    public List<LocalDate> getTrainingDates() {
        if (trainingDates != null) return new LinkedList<>(trainingDates);
        else return null;
    }

    public void addTrainingDate(LocalDate date) {
        if (trainingDates == null) trainingDates = new LinkedList<>();
        trainingDates.add(date);
    }

    private void setCompanies(Map<Long,Organization> companies) {
        this.companies = new HashMap<>(companies);
    }

    public Map<Long,Organization> getCompanies() {
        return new HashMap<>(companies);
    }

    public void addCompany(Organization organization) {
        if (organization == null) throw new NullPointerException("Organization musn't be null!");
        if (companies.containsKey(organization.getNIP())) throw new IllegalArgumentException("The given short name has been already used");
        if (!organization.containsEmployee(this)) {
            organization.addEmployee(this);
        }
        companies.put(organization.getNIP(), organization);
    }

    public Organization getCompany(Long nip) {
        if (nip == null || nip < 0) throw new NullPointerException("A NIP value must be provided!");
        return companies.getOrDefault(nip,null);
    }

    public boolean containsCompany(Long nip) {
        return companies.containsKey(nip);
    }

    private void setCompany(Organization organization) {
        if (companies == null) companies = new HashMap<>();
        if (organization == null) throw new NullPointerException("Organization musn't be null!");
        if (!organization.containsEmployee(this)) {
            organization.addEmployee(this);
        }
        companies.put(organization.getNIP(), organization);
    }

    public void removeCompany(Organization organization) {
        if (!companies.containsKey(organization.getNIP())) throw new IllegalArgumentException("No organization with the given shortName qualifier exists");
        if (organization.containsEmployee(this)) {
            organization.removeEmployee(this);
        }
        companies.remove(organization.getNIP());
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (this.department != null && this.department != department) {
            this.department.removeEmployee(this,department);
            //if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        } else {
            if (!department.contains(this)) {
                department.addEmployee(this);
            }
        }
        this.department = department;
    }

    public Position getPosition() {
        return position;
    }

    public String getId() {
        if (whole != null) return whole.getId();
        else return null;
    }

    public String getFirstName() {
        if (whole != null) return whole.getFirstName();
        else return null;
    }

    public String getSurname() {
        if (whole != null) return whole.getSurname();
        else return null;
    }

    public String getFulllName() {
        if (whole != null) return whole.getFullName();
        else return null;
    }

    public void setPosition(Position position) {
        if (position == null) throw new NullPointerException("Position can't be null");
        if (this.position != null) {
            this.position.removeEmployee(this);
        }
        this.position = position;
        position.addEmployee(this);
    }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("employees.dat"));
            objectOutputStream.writeObject(Employee.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("employees.dat"));
            Employee.extent = (Set<Employee>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printTrainingReport() {
        System.out.println("ID\tFirst name\tSurname\tMost recent training date\n");
        extent.forEach(e-> System.out.println(e.getId()+"\t"+e.getFirstName()+"\t"+e.getSurname()+"\t"+e.getMostRecentTrainingDate()));
    }

    public Person getPerson() {
        return whole;
    }

    public void setPerson(Person whole) {
        if (whole == null) {
            if (this.whole != null) {
                this.whole.removeEmployee(this);
                this.department.removeEmployee(this);
            }
        } else {
            if (this.whole != whole) {
                if (this.whole != null) throw new IllegalArgumentException("This employee already belongs to a Person!");
                if (whole.getEmployee() != null && whole.getEmployee() != this) throw new IllegalArgumentException("An employee object is already assigned to this Person");
                this.whole = whole;
                if (whole.getEmployee() != this) {
                    whole.setEmployee(this);
                }
            }
        }
    }

    public static List<Employee> exportList() {
        return new ArrayList<>(extent);
    }

    @Override
    public String toString() {
        return whole.getFullName() +" " + whole.getId();
    }

}
