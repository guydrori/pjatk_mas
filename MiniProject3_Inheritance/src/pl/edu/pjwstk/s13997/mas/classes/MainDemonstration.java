package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.classes.employee.EmployeeBuilder;

public class MainDemonstration {
    public static void main(String[] args) {
        //Example of dynamic inheritance
        Organization organization = new Organization(3627967698L,"Testowa Firma Sp. z o.o.",new Address("Marszałkowska 1","Warszawa","00-000"));
        Company company = new Company(organization,9237638925L);
        organization.changeType(new SoleProprietorship("ABC002","Ann","Smith"));
        //Example of multi inheritance
        SoleProprietorship soleProprietorship = organization.getSoleProprietorship();
        System.out.println("Organization inherited: " + soleProprietorship.getOrganization());
        System.out.println("Person inherited: " + soleProprietorship);

        //Example of overlapping inheritance
        Position position = new Position("Independent accountant");
        Person person = new Person("ABC001","John","Doe");
        Department department = new Department("Accounting","Poland");
        department.setOrganization(organization);
        Employee employee = EmployeeBuilder.build(person,organization,department,position);
        User user = new User(person,new Role("ADMIN",Role.Permissions.ADMIN),"j.doe","test"); //Example of multi-aspect inheritance
        System.out.println("\nEmployee person: " + employee.getPerson());
        System.out.println("Person user: " + person.getUser());

        //Example of polymorphism
        Entry dataStore = new PersonalDataStore("E-commerce","Admin 1:\nJohn Doe\nIT Manager\nTestowa Firma Sp. z o.o.\nj.doe@testowafirma.pl","Customer data required for realizing online orders","All e-commerce clients","Name, Address, Email","SSL Encryption, server firewall");
        Entry securityBreach = new SecurityBreach("E-commerce breach","Customer data leak","Increased server security",SecurityBreach.Status.SIGNIFICANT);
        System.out.println(dataStore.getIdentifyingDetails());
        System.out.println(securityBreach.getIdentifyingDetails());
    }
}
