package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pjwstk.s13997.mas.classes.Role;
import pl.edu.pjwstk.s13997.mas.repositories.RoleRepository;

@RestController
public class RoleController {
    @Autowired
    private RoleRepository repository;

    @RequestMapping(value = "/role/insert",method = RequestMethod.POST)
    public Role insert(@RequestParam(name="name") String name,@RequestParam("permissions") String permissions) {
        Role role = new Role(name,Role.Permissions.valueOf(permissions));
        repository.save(role);
        return role;
    }

    @RequestMapping("/role/list")
    public Iterable<Role> list() {
        return repository.findAll();
    }

    @RequestMapping("/role/{id}")
    public Role getAttachment(@PathVariable("id") int id) {
        return repository.findById(id).orElse(null);
    }
}
