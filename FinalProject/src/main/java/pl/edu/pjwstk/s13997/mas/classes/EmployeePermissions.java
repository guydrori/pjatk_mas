package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import javax.persistence.Basic;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="employee_permissions")
public class EmployeePermissions implements Serializable {
    @EmbeddedId EmployeePermissionsId id;
    @Basic(optional = false)
    private Position.Permissions defaultPermissions;
    //private static Set<Map.Entry<PersonalDataStore,Position>> pairSet = new HashSet<>();
    //private static Set<PositionPermissions> extent = new HashSet<>();

    public EmployeePermissions() {

    }

    public EmployeePermissions(PersonalDataStore personalDataStore, Employee employee, Position.Permissions permissions) {
        if (id == null) id = new EmployeePermissionsId();
        setPersonalDataStore(personalDataStore);
        setEmployee(employee);
        setDefaultPermissions(permissions);
    }

    public EmployeePermissions(PersonalDataStore personalDataStore, Employee employee) {
        if (id == null) id = new EmployeePermissionsId();
        setPersonalDataStore(personalDataStore);
        setEmployee(employee);
    }

    public void setPersonalDataStore(PersonalDataStore personalDataStore) {
        if (personalDataStore == null) throw new NullPointerException("Personal data store cannot be null!");
        this.id.personalDataStore = personalDataStore;
    }

    public void setEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee can't be null");
        this.id.employee = employee;
    }


    public PersonalDataStore getPersonalDataStore() {
        return id.personalDataStore;
    }

    public Employee getEmployee() {
        return id.employee;
    }

    public Position.Permissions getDefaultPermissions() {
        return defaultPermissions;
    }

    public void setDefaultPermissions(Position.Permissions permissions) {
        this.defaultPermissions = permissions;
    }

//    public static Set<PersonalDataStore> getPersonalDataStores(Position position) {
//        return pairSet.stream().filter(e->e.getValue().equals(position)).map(Map.Entry::getKey).collect(Collectors.toSet());
//    }

//    public static boolean pairExists(PersonalDataStore personalDataStore, Position position) {
//        return pairSet.contains(new Map.Entry<PersonalDataStore, Position>() {
//            @Override
//            public PersonalDataStore getKey() {
//                return personalDataStore;
//            }
//
//            @Override
//            public Position getValue() {
//                return position;
//            }
//
//            @Override
//            public Position setValue(Position value) {
//                return null;
//            }
//        });
//    }
//
//    public static void remove(PositionPermissions permissions) {
//        extent.remove(permissions);
//        pairSet.remove(new Map.Entry<PersonalDataStore, Position>() {
//            @Override
//            public PersonalDataStore getKey() {
//                return permissions.personalDataStore;
//            }
//
//            @Override
//            public Position getValue() {
//                return permissions.position;
//            }
//
//            @Override
//            public Position setValue(Position value) {
//                return null;
//            }
//        });
//    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Personal Data Store: ").append(id.personalDataStore.getName()).append(", Employee: ").append(id.employee.getFullName()).append(", Permissions: ").append(defaultPermissions.name());
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeePermissions that = (EmployeePermissions) o;
        return Objects.equals(id, that.id) &&
                defaultPermissions == that.defaultPermissions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, defaultPermissions);
    }
}
