package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.interfaces.IOrganization;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name="sole_proprietorship")
public class SoleProprietorship extends Person implements IOrganization {
    @OneToOne(optional = false)
    private Organization whole;
    public SoleProprietorship(Organization whole, String id, String firstName, String surname) {
        super(id, firstName, surname);
        setOrganization(whole);
    }

    public SoleProprietorship(String id, String firstName, String surname) {
        super(id, firstName, surname);
    }

    public SoleProprietorship() {
    }

    public IOrganization getOrganization() {
        return whole;
    }

    public void setOrganization(Organization whole) {
        if (whole == null) {
            if (this.whole != null) {
                this.whole.removeSoleProprietorship(this);
            }
        } else {
            if (this.whole != whole) {
                if (this.whole != null) throw new IllegalArgumentException("This employee already belongs to a Person!");
                if (whole.getSoleProprietorship() != null && whole.getSoleProprietorship() != this) throw new IllegalArgumentException("An employee object is already assigned to this Person");
                this.whole = whole;
                if (whole.getSoleProprietorship() != this) {
                    whole.setSoleProprietorship(this);
                }
            }
        }
    }

    @Override
    public void setAddress(Address address) {
        if (whole != null) whole.setAddress(address);
        else throw new NullPointerException();
    }

    @Override
    public Address getAddress() {
        if (whole != null) return whole.getAddress();
        else throw new NullPointerException();
    }

    @Override
    public void setName(String name) {
        if (whole != null) whole.setName(name);
        else throw new NullPointerException();
    }

    @Override
    public String getName() {
        if (whole != null) return whole.getName();
        else throw new NullPointerException();
    }

    @Override
    public void setNIP(long NIP) {
        if (whole != null) whole.setNIP(NIP);
        else throw new NullPointerException();
    }

    @Override
    public long getNIP() {
        if (whole != null) return whole.getNIP();
        else throw new NullPointerException();
    }

    @Override
    public Set<Department> getDepartments() {
        if (whole != null) return whole.getDepartments();
        else throw new NullPointerException();
    }
}
