package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.edu.pjwstk.s13997.mas.classes.Attachment;
import pl.edu.pjwstk.s13997.mas.repositories.AttachmentRepository;
import pl.edu.pjwstk.s13997.mas.storage.StorageService;

import java.util.List;

@RestController
public class AttachmentController {
    @Autowired
    private AttachmentRepository repository;

    private final StorageService storageService;

    @Autowired
    public AttachmentController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping(value = "/attachment/insert",method = RequestMethod.POST)
    public Attachment insert(@RequestParam(name="fileName") String fileName, @RequestParam(name="url",required = false) String url,
                             @RequestParam(name="file",required = false) MultipartFile file) {
        if (file == null) {
            Attachment attachment = new Attachment(fileName,url);
            repository.save(attachment);
            return attachment;
        } else if (url == null) {
            storageService.store(file);
            Attachment attachment = new Attachment(fileName,fileName);
            repository.save(attachment);
            return attachment;
        }
        return null;
    }

    @RequestMapping("/attachment/list")
    public Iterable<Attachment> list() {
        return repository.findAll();
    }

    @RequestMapping("/attachment/{id}")
    public Attachment getAttachment(@PathVariable("id") String id) {
        return repository.findById(id).orElse(null);
    }

    @RequestMapping("/attachment/file/{filename}")
    public ResponseEntity<Resource> getFile(@PathVariable("filename") String fileName) {
        if (fileName == null || fileName.isEmpty()) throw new NullPointerException();

        Resource file = storageService.loadAsResource(fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + file.getFilename()+"\"").contentType(MediaType.TEXT_PLAIN).body(file);
    }

}
