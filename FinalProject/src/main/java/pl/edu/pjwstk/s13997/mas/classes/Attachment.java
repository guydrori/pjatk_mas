package pl.edu.pjwstk.s13997.mas.classes;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table( name="attachment")

public class Attachment {
    @Id
    private String fileName;
    @Column(unique = true)
    private String URL;
    @ManyToOne
    @JsonIgnore
    private PersonalDataStore personalDataStore;
    @Transient
    @JsonIgnore
    private SecurityBreach securityBreach;

    public Attachment() {

    }

    public Attachment(String fileName, String URL) {
        setFileName(fileName);
        setSecurityBreach(securityBreach);
        setURL(URL);
    }

    public Attachment(SecurityBreach securityBreach, String fileName, String URL) {
        setFileName(fileName);
        setSecurityBreach(securityBreach);
        setURL(URL);
    }

    public Attachment(PersonalDataStore personalDataStore, String fileName, String URL) {
        setFileName(fileName);
        setPersonalDataStore(personalDataStore);
        setURL(URL);
    }

    public void setFileName(String fileName) {
        if(fileName == null || fileName.isEmpty()) throw new IllegalArgumentException("A file name must be provided");
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setURL(String URL) {
        if(URL == null || URL.isEmpty()) throw new IllegalArgumentException("A URL must be provided");
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }

    public PersonalDataStore getPersonalDataStore() {
        return personalDataStore;
    }

    public void setPersonalDataStore(PersonalDataStore personalDataStore) {
        if (personalDataStore == null) {
            this.personalDataStore = null;
            return;
        }
        if (securityBreach != null) throw new IllegalArgumentException("This attachment is already assigned to a security breach!");
        if (this.personalDataStore != null) {
            this.personalDataStore.removeAttachment(this);
        }
        if (!personalDataStore.contains(this)) {
            personalDataStore.addAttachment(this);
        }
        this.personalDataStore = personalDataStore;
    }

    public boolean canAssignPersonalDataStore() {
        return securityBreach == null;
    }

    public SecurityBreach getSecurityBreach() {
        return securityBreach;
    }

    public void setSecurityBreach(SecurityBreach securityBreach) {
        if (securityBreach == null) {
            this.securityBreach = null;
            return;
        }
        if (personalDataStore != null) throw new IllegalArgumentException("This attachment is already assigned to a personal data store!");
        if (this.securityBreach != null) {
           // this.securityBreach.removeAttachment(this);
        }
//        if (!securityBreach.contains(this)) {
//            securityBreach.addAttachment(this);
//        }
        this.securityBreach = securityBreach;
    }

    public boolean canAssignSecurityBreach() {
        return personalDataStore == null;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("File name: ").append(getFileName()).append(" URL: ").append(getURL());
        return stringBuilder.toString();
    }
}
