package pl.edu.pjwstk.s13997.mas.classes;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="class_attribute")
public class ClassAttribute implements Serializable {
    @EmbeddedId ClassAttributeId id;

    @Type(type="serializable")
    public Object value;

    public ClassAttribute() {

    }

    public ClassAttribute(String className, String attributeName, Object value) throws ClassNotFoundException,NoSuchFieldException {
        setClassName(className);
        setAttributeName(attributeName);
        setValue(value);
    }

    public String getClassName() {
        return id.className;
    }

    public void setClassName(String className) throws ClassNotFoundException{
        if (className == null || className.isEmpty()) throw new NullPointerException();
        if (Class.forName(className) == null) throw new IllegalArgumentException("Error finding class");
        this.id.className = className;
    }

    public String getAttributeName() {
        return id.attributeName;
    }

    public void setAttributeName(String attributeName) throws ClassNotFoundException,NoSuchFieldException {
        if (attributeName == null || attributeName.isEmpty()) throw new NullPointerException();
        if (Class.forName(id.className).getDeclaredField(attributeName) == null) throw new IllegalArgumentException("Attribute loading error");
        this.id.attributeName = attributeName;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassAttribute attribute = (ClassAttribute) o;
        return Objects.equals(id, attribute.id) &&
                Objects.equals(value, attribute.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, value);
    }
}
