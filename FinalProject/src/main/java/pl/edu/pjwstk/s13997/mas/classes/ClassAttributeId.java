package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ClassAttributeId implements Serializable {
    @Column(name="class_name")
    public String className;

    @Column(name="attribute_name")
    public String attributeName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClassAttributeId that = (ClassAttributeId) o;
        return Objects.equals(className, that.className) &&
                Objects.equals(attributeName, that.attributeName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(className, attributeName);
    }
}
