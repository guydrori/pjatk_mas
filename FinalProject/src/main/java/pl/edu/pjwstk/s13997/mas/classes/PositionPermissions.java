package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name="position_permissions")
public class PositionPermissions implements Serializable {
    @EmbeddedId PositionPermissionsId id;
    @Basic(optional = false)
    private Position.Permissions defaultPermissions;
    //private static Set<Map.Entry<PersonalDataStore,Position>> pairSet = new HashSet<>();
    //private static Set<PositionPermissions> extent = new HashSet<>();

    public PositionPermissions() {

    }

    public PositionPermissions(PersonalDataStore personalDataStore, Position position, Position.Permissions permissions) {
        if (id == null) id = new PositionPermissionsId();
        setPersonalDataStore(personalDataStore);
        setPosition(position);
        setDefaultPermissions(permissions);
    }

    public PositionPermissions(PersonalDataStore personalDataStore, Position position) {
        if (id == null) id = new PositionPermissionsId();
        setPersonalDataStore(personalDataStore);
        setPosition(position);
    }

    public void setPersonalDataStore(PersonalDataStore personalDataStore) {
        if (personalDataStore == null) throw new NullPointerException("Personal data store cannot be null!");
        this.id.personalDataStore = personalDataStore;
    }

    public void setPosition(Position position) {
        if (position == null) throw new NullPointerException("Position cannot be null");
        this.id.position = position;
    }

    public PersonalDataStore getPersonalDataStore() {
        return id.personalDataStore;
    }

    public Position getPosition() {
        return id.position;
    }

    public Position.Permissions getDefaultPermissions() {
        return defaultPermissions;
    }

    public void setDefaultPermissions(Position.Permissions permissions) {
        this.defaultPermissions = permissions;
    }

//    public static Set<PersonalDataStore> getPersonalDataStores(Position position) {
//        return pairSet.stream().filter(e->e.getValue().equals(position)).map(Map.Entry::getKey).collect(Collectors.toSet());
//    }

//    public static boolean pairExists(PersonalDataStore personalDataStore, Position position) {
//        return pairSet.contains(new Map.Entry<PersonalDataStore, Position>() {
//            @Override
//            public PersonalDataStore getKey() {
//                return personalDataStore;
//            }
//
//            @Override
//            public Position getValue() {
//                return position;
//            }
//
//            @Override
//            public Position setValue(Position value) {
//                return null;
//            }
//        });
//    }
//
//    public static void remove(PositionPermissions permissions) {
//        extent.remove(permissions);
//        pairSet.remove(new Map.Entry<PersonalDataStore, Position>() {
//            @Override
//            public PersonalDataStore getKey() {
//                return permissions.personalDataStore;
//            }
//
//            @Override
//            public Position getValue() {
//                return permissions.position;
//            }
//
//            @Override
//            public Position setValue(Position value) {
//                return null;
//            }
//        });
//    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Personal Data Store: ").append(id.personalDataStore.getName()).append(", Position: ").append(id.position.getName()).append(", Permissions: ").append(defaultPermissions.name());
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PositionPermissions that = (PositionPermissions) o;
        return Objects.equals(id.personalDataStore, that.id.personalDataStore) &&
                Objects.equals(id.position, that.id.position) &&
                defaultPermissions == that.defaultPermissions;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id.personalDataStore, id.position, defaultPermissions);
    }
}
