package pl.edu.pjwstk.s13997.mas.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.edu.pjwstk.s13997.mas.classes.Role;

public interface RoleRepository extends CrudRepository<Role,Integer> {
}
