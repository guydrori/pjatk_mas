package pl.edu.pjwstk.s13997.mas.classes;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.classes.interfaces.IOrganization;
import pl.edu.pjwstk.s13997.mas.persistence.ClassAttributePersistence;
import pl.edu.pjwstk.s13997.mas.repositories.OrganizationRepository;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Component
@Table(name = "organization")
public class Organization implements Serializable,IOrganization {
    @Id
    @Column(name="NIP",unique = true,length=10)
    private long NIP;
    @OneToOne(targetEntity = Company.class,cascade=CascadeType.ALL)
    @JsonIgnore
    private Company company;
    @OneToOne(targetEntity = SoleProprietorship.class,cascade = CascadeType.ALL)
    private SoleProprietorship soleProprietorship;
    @Basic(optional = false)
    @Column(name="NAME")
    private String name;
    @ManyToOne
    @JoinColumn(name="ADDRESS_ID",foreignKey = @ForeignKey(name="ADDRESS_ID_FK"))
    private Address address; //Complex attribute
    @OneToMany(cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Department> departments;
    @ManyToMany
    @JsonBackReference
    private Set<Employee> employees;
    //private static Set<Organization> extent = new HashSet<>();

    @Transient
    private static OrganizationRepository organizationRepository;

    public Organization(long NIP, String name, Address address, Collection<Department> departments) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        setDepartments(departments);
        employees = new HashSet<>();
        //extent.add(this);
    }

    public Organization(long NIP, String name, Address address, Collection<Department> departments, Collection<Employee> employees) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        setDepartments(departments);
        setEmployees(employees);
        //extent.add(this);
    }

    public Organization(long NIP, String name, Address address) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        departments = new HashSet<>();
        employees = new HashSet<>();
        //extent.add(this);
    }

    public Organization() {
    }

    public void setAddress(Address address) {
        if (address == null) throw new NullPointerException("The address can't be null");
        this.address = address;
    }

    public Address getAddress() { return address; }

    public void setName(String name) {
        if(name == null || name.isEmpty()) throw new IllegalArgumentException("A name must be provided");
        this.name = name;
    }

    public String getName() { return name; }

    public void setNIP(long NIP) {
        if (String.valueOf(NIP).length() != 10) throw new IllegalArgumentException("NIP must have 10 digits!");
        //if (extent.stream().anyMatch(c->c.NIP == NIP)) throw new IllegalArgumentException("A company with the given NIP exists");
        this.NIP = NIP;
    }

    public long getNIP() { return NIP; }

//    public static void saveExtent() {
//        try {
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("companies.dat"));
//            objectOutputStream.writeObject(Organization.extent);
//            objectOutputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void loadExtent() {
//        try {
//            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("companies.dat"));
//            Organization.extent = (Set<Organization>) objectInputStream.readObject();
//            objectInputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    public Set<Organization> getExtent() {
//        return new HashSet<>(extent);
//    }

    protected void addDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department cannot be null");
        if (anyContains(department)) throw new IllegalArgumentException("The department is already assigned to a company, please remove it first");
        //if (extent.stream().anyMatch(c->c.departments.contains(department))) throw new IllegalArgumentException("The department is already assigned to a company, please remove it first");
        departments.add(department);
    }

    protected void removeDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department cannot be null");
        departments.remove(department);
    }

    public Set<Department> getDepartments() {
        return new HashSet<>(departments);
    }

    private void setDepartments(Collection<Department> departments) {
        if (departments != null) {
            this.departments = new HashSet<>(departments);
        }
    }

    private void setEmployees(Collection<Employee> employees) {
        if (employees != null) {
            this.employees = new HashSet<>(employees);
            for(Employee e: employees) {
                if (!e.containsCompany(NIP)) {
                    e.addCompany(this);
                }
            }
        }
    }

    public Set<Employee> getEmployees() {
        return new HashSet<>(employees);
    }

    public void addEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee cannot be null");
        employees.add(employee);
        if (!employee.containsCompany(NIP)) {
            employee.addCompany(this);
        }
    }

    public void removeEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee cannot be null");
        employees.remove(employee);
        if (employee.containsCompany(NIP)) {
            employee.removeCompany(this);
        }
    }

    public boolean containsEmployee(Employee employee) {
        return employees.contains(employee);
    }

    public void removeCompany(Company company) {
        if (this.company != null && this.company == company) {
            this.company = null;
            company.setOrganization(null);
        }
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        if (company == null) throw new NullPointerException();
        if (company.getOrganiztaion() != null && company.getOrganiztaion() != this) throw new IllegalArgumentException("This company already belongs to an organization!");
        if (this.company != null) throw new IllegalArgumentException("A company object is already assigned to this Organization!");
        if (this.soleProprietorship != null) throw new RuntimeException("This object is a sole proprietorship");
        if (anyContains(company)) throw new IllegalArgumentException("This company already belongs to an organization!");
        this.company = company;
        company.setOrganization(this);
    }
    @JsonIgnore
    public String getKRS() {
        if (company != null) return company.getKRS();
        else throw new RuntimeException("This organization is not of a company type");
    }

    public void setKRS(String KRS) {
        if (company != null) company.setKRS(KRS);
        else throw new RuntimeException("This organization is not of a company type");
    }
    @JsonIgnore
    public SoleProprietorship getSoleProprietorship() {
        return soleProprietorship;
    }

    public void removeSoleProprietorship(SoleProprietorship soleProprietorship) {
        if (this.soleProprietorship != null && this.soleProprietorship == soleProprietorship) {
            this.soleProprietorship = null;
            soleProprietorship.setOrganization(null);
        }
    }
    @JsonIgnore
    public String getId() {
        if (soleProprietorship != null) return soleProprietorship.getId();
        else throw new RuntimeException("This organization is not a sole proprietorship");
    }
    @JsonIgnore
    public String getFirstName() {
        if (soleProprietorship != null) return soleProprietorship.getFirstName();
        else throw new RuntimeException("This organization is not a sole proprietorship");
    }
    @JsonIgnore
    public String getSurname() {
        if (soleProprietorship != null) return soleProprietorship.getSurname();
        else throw new RuntimeException("This organization is not a sole proprietorship");
    }
    @JsonIgnore
    public String getFullName() {
        if (soleProprietorship != null) return soleProprietorship.getFullName();
        else throw new RuntimeException("This organization is not a sole proprietorship");
    }

    public void setSoleProprietorship(SoleProprietorship soleProprietorship) {
        if (soleProprietorship == null) throw new NullPointerException();
        if (soleProprietorship.getOrganization() != null && soleProprietorship.getOrganization() != this) throw new IllegalArgumentException("This company already belongs to an organization!");
        if (this.soleProprietorship != null) throw new IllegalArgumentException("A company object is already assigned to this Organization!");
        if (this.company != null) throw new RuntimeException("This object is a company");
        if (anyContains(soleProprietorship)) throw new IllegalArgumentException("This company already belongs to an organization!");
        this.soleProprietorship = soleProprietorship;
        soleProprietorship.setOrganization(this);
    }

    public void changeType(Company company) {
        if (this.company != null) throw new IllegalArgumentException("This object is already a company");
        removeSoleProprietorship(this.soleProprietorship);
        setCompany(company);
    }

    public void changeType(SoleProprietorship soleProprietorship) {
        if (this.soleProprietorship != null) throw new IllegalArgumentException("This object is already a sole proprietorship");
        removeCompany(this.company);
        setSoleProprietorship(soleProprietorship);
    }

    private boolean anyContains(SoleProprietorship soleProprietorship) {
        Long count = organizationRepository.getSoleProprietorshipCount(soleProprietorship.getId());
        if (count > 0) return true;
        else return false;
    }

    private boolean anyContains(Company company) {
        Long count = organizationRepository.getCompanyCount(company.getKRS());
        if (count > 0) return true;
        else return false;
    }

    private boolean anyContains(Department department) {
        Long count = organizationRepository.getDepartmentCount(department.getId());
        if (count > 0) return true;
        else return false;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization organization = (Organization) o;
        return NIP == organization.NIP &&
                Objects.equals(name, organization.name) &&
                Objects.equals(address, organization.address);
    }

    @Override
    public String toString() {
        return "NIP: " + getNIP() + " Name: " + getName();
    }

    @Autowired
    public void setOrganizationRepository(OrganizationRepository repository) {
        Organization.organizationRepository = repository;
    }
}
