package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pjwstk.s13997.mas.classes.Address;
import pl.edu.pjwstk.s13997.mas.classes.Company;
import pl.edu.pjwstk.s13997.mas.classes.Organization;
import pl.edu.pjwstk.s13997.mas.repositories.AddressRepository;
import pl.edu.pjwstk.s13997.mas.repositories.CompanyRepository;
import pl.edu.pjwstk.s13997.mas.repositories.OrganizationRepository;


@RestController
public class CompanyController {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private AddressRepository addressRepository;

    @RequestMapping(value = "/company/insert",method = RequestMethod.POST)
    public Company insert(@RequestParam(name="nip") Long nip,@RequestParam(name="krs") String krs, @RequestParam(name="name") String name, @RequestParam(name="streetData") String streetData,@RequestParam(name="city") String city,
                          @RequestParam(name="postcode") String postcode) {
        Address address = new Address(streetData,city,postcode);
        addressRepository.save(address);
        Organization organization = new Organization(nip,name,address);
        organizationRepository.save(organization);
        Company company = new Company(organization,krs);
        companyRepository.save(company);
        organizationRepository.save(organization);
        return company;
    }

    @RequestMapping("/company/list")
    @CrossOrigin
    public Iterable<Company> list() {
        return companyRepository.findAll();
    }

    @RequestMapping("/company/{id}")
    public Company getCompany(@PathVariable("id") String id) {
        try {
            long nip = Long.parseLong(id);
            return organizationRepository.findById(nip).orElse(null).getCompany();
        } catch (NumberFormatException e) {
            return organizationRepository.findByName(id).orElse(null).getCompany();
        }
    }
}
