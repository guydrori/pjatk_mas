package pl.edu.pjwstk.s13997.mas.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.edu.pjwstk.s13997.mas.classes.Department;
import pl.edu.pjwstk.s13997.mas.classes.Organization;

public interface DepartmentRepository extends CrudRepository<Department,Integer> {
    Department findByName(String name);
    @Query("SELECT d FROM Department d WHERE d.organization =?1")
    Iterable<Department> findAllByOrganization(Organization organization);
}
