package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class Entry {
    @Id
    @GeneratedValue
    private int id;
    private LocalDateTime creationTime;

    public Entry() {
        creationTime = LocalDateTime.now();
    }

    public Entry(int id) {
        setId(id);
        creationTime = LocalDateTime.now();
    }

    public Entry(int id,LocalDateTime creationTime) {
        setCreationTime(creationTime);
        setId(id);
    }

    public Entry(LocalDateTime creationTime) {
        setCreationTime(creationTime);
    }

    private void setId(int id) {
        if (id > 0) {
            this.id = id;
        } else throw new NullPointerException();
    }

    public int getId() {
        return id;
    }

    private void setCreationTime(LocalDateTime creationTime) {
        if (creationTime == null) throw new NullPointerException("Creation time must not be null");
        this.creationTime = creationTime;
    }

    public LocalDateTime getCreationTime() { return creationTime; }

    public abstract void print();

    public abstract String getIdentifyingDetails();

}
