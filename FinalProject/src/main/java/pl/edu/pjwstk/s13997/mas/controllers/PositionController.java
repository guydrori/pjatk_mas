package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pjwstk.s13997.mas.classes.PersonalDataStore;
import pl.edu.pjwstk.s13997.mas.classes.Position;
import pl.edu.pjwstk.s13997.mas.classes.PositionPermissions;
import pl.edu.pjwstk.s13997.mas.repositories.PersonalDataStoreRepository;
import pl.edu.pjwstk.s13997.mas.repositories.PositionPermissionsRepository;
import pl.edu.pjwstk.s13997.mas.repositories.PositionRepository;


@RestController
public class PositionController {
    @Autowired
    private PositionRepository repository;

    @Autowired
    private PositionPermissionsRepository positionPermissionsRepository;

    @Autowired
    private PersonalDataStoreRepository personalDataStoreRepository;

    @RequestMapping(value = "/position/insert",method = RequestMethod.POST)
    public Position insert(@RequestParam(name="name") String name) {
        Position position = new Position(name);
        repository.save(position);
        return position;
    }

    @RequestMapping("/position/list")
    @CrossOrigin
    public Iterable<Position> list() {
        return repository.findAll();
    }

    @RequestMapping("/position/{id}")
    public Position getPositon(@PathVariable("id") String id) {
        return repository.findById(id).orElse(null);
    }

    @RequestMapping(value="/position/permissions/insert",method = RequestMethod.POST)
    @CrossOrigin
    public PositionPermissions insertPositionPermissions(@RequestParam(name="personalDataStoreId") int personalDataStoreId,
                                                         @RequestParam(name="position") String position,
                                                         @RequestParam(name="permissions") String permissions) {
        Position positionObject = repository.findById(position).orElse(null);
        PersonalDataStore personalDataStore = personalDataStoreRepository.findById(personalDataStoreId).orElse(null);
        Position.Permissions permissionsObject = Position.Permissions.valueOf(permissions);
        PositionPermissions positionPermissions = new PositionPermissions(personalDataStore,positionObject,permissionsObject);
        positionPermissionsRepository.save(positionPermissions);
        return positionPermissions;
    }
}
