package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import pl.edu.pjwstk.s13997.mas.classes.*;
import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.classes.employee.EmployeeBuilder;
import pl.edu.pjwstk.s13997.mas.repositories.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
public class EmployeeController {
    @Autowired
    private EmployeeRepository repository;

    @Autowired
    private DepartmentRepository deptRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private EmployeePermissionsRepository employeePermissionsRepository;

    @Autowired
    private PersonalDataStoreRepository personalDataStoreRepository;

    @RequestMapping(value = "/employee/insert",method = RequestMethod.POST)
    public Employee insert(@RequestParam(name="id") String id, @RequestParam(name="firstName") String name, @RequestParam(name="surname") String surname,
                           @RequestParam(name="department") int dept, @RequestParam(name="position") String position, @RequestParam(name="mostRecentTrainingDate",required = false)
                           @DateTimeFormat(iso=DateTimeFormat.ISO.DATE) LocalDate mostRecentTrainingDate) {
        Person person = new Person(id,name,surname);
        personRepository.save(person);
        Department department = deptRepository.findById(dept).orElse(null);
        Position positionObject = positionRepository.findById(position).orElse(null);
        Employee employee = null;
        if (mostRecentTrainingDate == null) {
            employee = EmployeeBuilder.build(person,department.getOrganization(),department,positionObject);
        } else {
            List<LocalDate> trainingDateList = new ArrayList<>();
            trainingDateList.add(mostRecentTrainingDate);
            employee = EmployeeBuilder.build(person,trainingDateList,department.getOrganization(),department,positionObject);
        }
        repository.save(employee);
        return employee;
    }

    @RequestMapping("/employee/list")
    public Iterable<Employee> list() {
        return repository.findAll();
    }

    @RequestMapping("/employee/list/{deptId}")
    @CrossOrigin
    public Iterable<Employee> list(@PathVariable("deptId") int deptId) {
        Department department = deptRepository.findById(deptId).orElse(null);
        return repository.findAllByDepartment(department);
    }

    @RequestMapping("/employee/{id}")
    public Employee getEmployee(@PathVariable("id") String id) {
        return repository.findById(id).orElse(null);
    }

    @RequestMapping(value="/employee/permissions/insert",method = RequestMethod.POST)
    @CrossOrigin
    public EmployeePermissions insertPositionPermissions(@RequestParam(name="personalDataStoreId") int personalDataStoreId,
                                                         @RequestParam(name="employee") String employeeId,
                                                         @RequestParam(name="permissions") String permissions) {
        Employee employee = repository.findById(employeeId).orElse(null);
        PersonalDataStore personalDataStore = personalDataStoreRepository.findById(personalDataStoreId).orElse(null);
        Position.Permissions permissionsObject = Position.Permissions.valueOf(permissions);
        EmployeePermissions employeePermissions = new EmployeePermissions(personalDataStore,employee,permissionsObject);
        employeePermissionsRepository.save(employeePermissions);
        return employeePermissions;
    }
}
