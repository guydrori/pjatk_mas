package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class EmployeePermissionsId implements Serializable {
    @ManyToOne
    PersonalDataStore personalDataStore;

    @ManyToOne
    Employee employee;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeePermissionsId that = (EmployeePermissionsId) o;
        return Objects.equals(personalDataStore, that.personalDataStore) &&
                Objects.equals(employee, that.employee);
    }

    @Override
    public int hashCode() {

        return Objects.hash(personalDataStore, employee);
    }
}
