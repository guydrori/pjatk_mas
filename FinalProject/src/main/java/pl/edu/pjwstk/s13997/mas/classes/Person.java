package pl.edu.pjwstk.s13997.mas.classes;

import org.hibernate.Session;
import org.hibernate.annotations.Formula;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;
import pl.edu.pjwstk.s13997.mas.persistence.ClassAttributePersistence;
import pl.edu.pjwstk.s13997.mas.repositories.EmployeeRepository;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Entity
@Component
@Table(name="person")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Person {
    @Id
    @Column(name="ID",length=20)
    private String id;
    @Column(name="FIRST_NAME")
    @Basic(optional = false)
    private String firstName;
    @Column(name="SURNAME")
    @Basic(optional = false)
    private String surname;
    @OneToOne
    @JoinColumn(name="EMPLOYEE_ID",foreignKey = @ForeignKey(name="EMPLOYEE_ID_FK"))
    private Employee employee;
    @Transient
    private User user;
    @Formula("CONCAT(FIRST_NAME,' ',SURNAME)")
    private String fullName;
    //private static Set<Person> extent = new HashSet<>();

    @Transient
    private static EmployeeRepository employeeRepository;

    public Person(String id,String firstName,String surname) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        //extent.add(this);
    }

    public Person() {
    }

    private void setId(String id) {
        if(id == null || id.isEmpty()) throw new IllegalArgumentException("An ID must be provided");
        //if(extent.stream().anyMatch(e->e.id.equals(id))) throw new IllegalArgumentException("An employee with the given ID exists");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        if(firstName == null || firstName.isEmpty()) throw new IllegalArgumentException("A first name must be provided");
        this.firstName = firstName;
    }

    public String getFirstName() { return firstName; }

    public void setSurname(String surname) {
        if(surname == null || surname.isEmpty()) throw new IllegalArgumentException("A first name must be provided");
        this.surname = surname;
    }

    public String getSurname() { return  surname; }

    //Derived attribute
    public String getFullName() { return fullName; }

    public void removeEmployee(Employee employee) {
        if (this.employee != null && this.employee == employee) {
            this.employee = null;
            employee.setPerson(null);
        }
    }

    public void setEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException();
        if (employee.getPerson() != null && employee.getPerson() != this) throw new IllegalArgumentException("This employee already belongs to a Person!");
        if (this.employee != null) throw new IllegalArgumentException("An employee object is already assigned to this Person!");
        if (anyContains(employee)) throw new IllegalArgumentException("This employee already belongs to a Person!");
        this.employee = employee;
        employee.setPerson(this);
    }

    private boolean anyContains(Employee employee) {
        Long count = employeeRepository.countById(id);
        if (count > 0) return true;
        else return false;
    }


    public Employee getEmployee() {
        return employee;
    }

    public Position getPosition() {
        if (employee != null) return employee.getPosition();
        else throw new RuntimeException("This object is not an Employee");
    }

    public Organization getCompany(Long nip) {
        if (employee != null) return employee.getCompany(nip);
        else throw new RuntimeException("This object is not an Employee");
    }

    public LocalDate getMostRecentTrainingDate() {
        if (employee != null) return employee.getMostRecentTrainingDate();
        else throw new RuntimeException("This object is not an Employee");
    }

    public List<LocalDate> getTrainingDates() {
        if (employee != null) return employee.getTrainingDates();
        else throw new RuntimeException("This object is not an Employee");
    }

    public Map<Long,Organization> getCompanies() {
        if (employee != null) return employee.getCompanies();
        else throw new RuntimeException("This object is not an Employee");
    }

    public Department getDepartment() {
        if (employee != null) return employee.getDepartment();
        else throw new RuntimeException("This object is not an Employee");
    }

    public void removeUser(User user) {
        if (this.user != null && this.user == user) {
            this.user = null;
            user.setPerson(null);
        }
    }


    public void setUser(User user) {
        if (user == null) throw new NullPointerException();
        if (user.getPerson() != null && user.getPerson() != this) throw new IllegalArgumentException("This user already belongs to a Person!");
        if (this.user != null) throw new IllegalArgumentException("A user object is already assigned to this Person!");
//        if (extent.stream().anyMatch(p-> {
//                if(p.getUser() != null) return p.getUser().equals(user);
//                else return false;
//        })) throw new IllegalArgumentException("This user already belongs to a Person!");
        this.user = user;
        user.setPerson(this);
    }


    public User getUser() {
        return user;
    }

    public Role getRole() {
        if (user != null) return user.getRole();
        else throw new RuntimeException("This object is not a User");
    }

    public byte[] getPassword() {
        if (user != null) return user.getPassword();
        else throw new RuntimeException("This object is not a User");
    }

    public void setPassword(String password) {
        if (user != null) user.setPassword(password);
        else throw new RuntimeException("This object is not a User");
    }

    public String getUsername() {
        if (user != null) return user.getUsername();
        else throw new RuntimeException("This object is not a User");
    }

//    public static void saveExtent() {
//        try {
//            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("employees.dat"));
//            objectOutputStream.writeObject(extent);
//            objectOutputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void loadExtent() {
//        try {
//            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("employees.dat"));
//            extent = (Set<Person>) objectInputStream.readObject();
//            objectInputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(id, person.id) &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(surname, person.surname);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, firstName, surname);
    }

    @Override
    public String toString() {
        return "ID: " + id + " Name: " + firstName + " Surname: " +surname;
    }

    @Autowired
    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        Person.employeeRepository = employeeRepository;
    }
}
