package pl.edu.pjwstk.s13997.mas.persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pl.edu.pjwstk.s13997.mas.classes.ClassAttribute;
import pl.edu.pjwstk.s13997.mas.repositories.ClassAttributeRepository;

import java.lang.reflect.Field;
import java.util.List;
@Component
public class ClassAttributePersistence {
    @Autowired
    private ClassAttributeRepository repository;

    public void persistClassAttribute(String className, String attributeName, Object value) {
        try {
            ClassAttribute attribute = new ClassAttribute(className,attributeName,value);
            repository.save(attribute);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
