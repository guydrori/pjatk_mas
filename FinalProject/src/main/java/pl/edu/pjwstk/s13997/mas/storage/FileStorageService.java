package pl.edu.pjwstk.s13997.mas.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Component
public class FileStorageService implements StorageService {

    private static final Path folderPath =Paths.get("filesDir");

    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public void init() {
        try {
            Files.createDirectories(folderPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void store(MultipartFile file) {
        Path filePath = folderPath.resolve(file.getOriginalFilename());
        if (!file.isEmpty()) {
            try {
                Files.write(filePath,file.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Stream<Path> loadAll() {
        return null;
    }

    @Override
    public Path load(String filename) {
        Path filePath = folderPath.resolve(filename);
        if (Files.exists(filePath)) {
            try {
                return filePath;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Resource loadAsResource(String filename) {
        Path filePath = folderPath.resolve(filename);
        if (Files.exists(filePath)) {
            try {
                String resourceURL = "file:".concat(filePath.toAbsolutePath().toString());
                return resourceLoader.getResource(resourceURL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void deleteAll() {
       try {
           if (Files.exists(folderPath)) {
               Files.list(folderPath).forEach(p-> {
                   try {
                       Files.deleteIfExists(p);
                   } catch (IOException e) {
                       e.printStackTrace();
                   }
               });
               Files.delete(folderPath);
           }
       } catch (Exception e) {
           e.printStackTrace();
       }
    }
}
