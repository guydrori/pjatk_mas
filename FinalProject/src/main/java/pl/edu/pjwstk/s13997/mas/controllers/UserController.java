package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pjwstk.s13997.mas.classes.Person;
import pl.edu.pjwstk.s13997.mas.classes.Role;
import pl.edu.pjwstk.s13997.mas.classes.User;
import pl.edu.pjwstk.s13997.mas.repositories.PersonRepository;
import pl.edu.pjwstk.s13997.mas.repositories.RoleRepository;
import pl.edu.pjwstk.s13997.mas.repositories.UserRepository;

import java.util.Arrays;

@RestController
public class UserController {

    @Autowired
    UserRepository repository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    RoleRepository roleRepository;

    @RequestMapping("/user/insert")
    public User insert(@RequestParam("username") String username,@RequestParam(name="password") String password,@RequestParam(name="empId") String employeeId,
                        @RequestParam("roleId") int roleId) throws Exception {
        if (password == null || password.length() == 0) throw new NullPointerException();
        Role role = roleRepository.findById(roleId).orElse(null);
        Person person = personRepository.findById(employeeId).orElse(null);
        User user = new User(person,role,username,password);
        repository.save(user);
        return user;
    }

    @RequestMapping("/user/list")
    public Iterable<User> list() {
        return repository.findAll();
    }

    @RequestMapping("/user/{id}")
    public User getUser(@PathVariable(name = "id") String id) {
        return repository.findById(id).orElse(null);
    }

    @RequestMapping("/user/login")
    public boolean login(@RequestParam("username") String username,@RequestParam(name="password") String password) throws Exception {
        if (password == null || password.length() == 0) throw new NullPointerException();
        User user = repository.findByUsername(username).orElse(null);
        byte[] encryptedPassword = User.hashPassword(password,user.getSalt());
        return Arrays.equals(encryptedPassword,user.getPassword());
    }

}
