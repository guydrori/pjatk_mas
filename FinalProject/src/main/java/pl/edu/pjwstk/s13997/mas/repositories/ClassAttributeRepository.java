package pl.edu.pjwstk.s13997.mas.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.edu.pjwstk.s13997.mas.classes.ClassAttribute;
import pl.edu.pjwstk.s13997.mas.classes.ClassAttributeId;

public interface ClassAttributeRepository extends CrudRepository<ClassAttribute,ClassAttributeId> {

}
