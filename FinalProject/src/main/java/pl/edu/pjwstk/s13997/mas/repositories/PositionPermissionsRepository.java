package pl.edu.pjwstk.s13997.mas.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.edu.pjwstk.s13997.mas.classes.Position;
import pl.edu.pjwstk.s13997.mas.classes.PositionPermissions;
import pl.edu.pjwstk.s13997.mas.classes.PositionPermissionsId;

import java.util.List;

public interface PositionPermissionsRepository extends CrudRepository<PositionPermissions,PositionPermissionsId> {
    @Query("SELECT s FROM PositionPermissions s WHERE s.id.position =?1")
    List<PositionPermissions> findByPosition(Position position);
}
