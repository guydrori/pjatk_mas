package pl.edu.pjwstk.s13997.mas.classes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.edu.pjwstk.s13997.mas.classes.interfaces.IOrganization;

import javax.persistence.*;
import java.util.Set;
import java.util.regex.Pattern;

@Entity
@Table(name="company")
public class Company implements IOrganization {
    @OneToOne(targetEntity = Organization.class,optional = false)
    @JsonIgnore
    private Organization whole;
    @Id
    @Column(name="KRS",length = 10)
    private String KRS;

    @Transient
    private static String KRS_PATTERN = "\\d{10}";

    public Company(Organization whole, String krs) {
        setOrganization(whole);
        setKRS(krs);
    }

    public Company(String krs) {
        setKRS(krs);
    }

    public Company() {
    }

    public String getKRS() {
        return KRS;
    }

    public void setKRS(String KRS) {
        if (!Pattern.matches(KRS_PATTERN,KRS)) throw new IllegalArgumentException("KRS number must have 10 digits!");
        //if (extent.stream().anyMatch(c->c.KRS == KRS)) throw new IllegalArgumentException("A company with the given KRS number exists");
        this.KRS = KRS;
    }
    @JsonIgnore
    public IOrganization getOrganiztaion() {
        return whole;
    }

    public void setOrganization(Organization whole) {
        if (whole == null) {
            if (this.whole != null) {
                this.whole.removeCompany(this);
            }
        } else {
            if (this.whole != whole) {
                if (this.whole != null) throw new IllegalArgumentException("This company already belongs to an organization!");
                if (whole.getCompany() != null && whole.getCompany() != this) throw new IllegalArgumentException("An Organization object is already assigned to this Company");
                this.whole = whole;
                if (whole.getCompany() != this) {
                    whole.setCompany(this);
                }
            }
        }
    }

    @Override
    public void setAddress(Address address) {
        if (whole != null) whole.setAddress(address);
        else throw new NullPointerException();
    }

    @Override
    public Address getAddress() {
        if (whole != null) return whole.getAddress();
        else throw new NullPointerException();
    }

    @Override
    public void setName(String name) {
        if (whole != null) whole.setName(name);
        else throw new NullPointerException();
    }

    @Override
    public String getName() {
        if (whole != null) return whole.getName();
        else throw new NullPointerException();
    }

    @Override
    public void setNIP(long NIP) {
        if (whole != null) whole.setNIP(NIP);
        else throw new NullPointerException();
    }

    @Override
    public long getNIP() {
        if (whole != null) return whole.getNIP();
        else throw new NullPointerException();
    }

    @Override
    public Set<Department> getDepartments() {
        if (whole != null) return whole.getDepartments();
        else throw new NullPointerException();
    }

    @Override
    public String toString() {
        return String.valueOf(getKRS());
    }
}
