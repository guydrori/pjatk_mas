package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pjwstk.s13997.mas.classes.Department;
import pl.edu.pjwstk.s13997.mas.classes.Organization;
import pl.edu.pjwstk.s13997.mas.repositories.DepartmentRepository;
import pl.edu.pjwstk.s13997.mas.repositories.OrganizationRepository;


@RestController
public class DepartmentController {
    @Autowired
    private DepartmentRepository repository;

    @Autowired
    private OrganizationRepository organizationRepository;

    @RequestMapping(value = "/department/insert",method = RequestMethod.POST)
    public Department insert(@RequestParam(name="name") String name, @RequestParam(name="region") String region,@RequestParam(name="company") Long companyNip) {
        Organization organization = organizationRepository.findById(companyNip).orElse(null);
        Department department = new Department(name,region,organization);
        repository.save(department);
        return department;
    }

    @RequestMapping("/department/list")
    public Iterable<Department> list() {
        return repository.findAll();
    }

    @RequestMapping("/department/list/{org}")
    @CrossOrigin
    public Iterable<Department> list(@PathVariable("org") Long nip) {
        Organization organization = organizationRepository.findById(nip).orElse(null);
        return repository.findAllByOrganization(organization);
    }

    @RequestMapping("/department/{id}")
    public Department getDepartment(@PathVariable("id") String id) {
        try {
            int idInt = Integer.parseInt(id);
            return repository.findById(idInt).orElse(null);
        } catch (NumberFormatException e) {
            return repository.findByName(id);
        }
    }
}
