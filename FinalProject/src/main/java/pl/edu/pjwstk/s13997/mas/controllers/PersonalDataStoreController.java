package pl.edu.pjwstk.s13997.mas.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.edu.pjwstk.s13997.mas.classes.PersonalDataStore;
import pl.edu.pjwstk.s13997.mas.repositories.PersonalDataStoreRepository;

@RestController
public class PersonalDataStoreController {
    @Autowired
    private PersonalDataStoreRepository repository;

    @RequestMapping(value = "/personal-data-store/insert",method = RequestMethod.POST)
    @CrossOrigin
    public PersonalDataStore insert(@RequestParam(name="name") String name, @RequestParam(name="location")String location, @RequestParam(name="adminDetails")String adminDetails,
                                    @RequestParam(name="processingObjectives")String processingObjectives, @RequestParam(name="targetPeopleCategoryDescription")String targetPeopleCategoryDescription,
                                    @RequestParam(name="personalDataCategory")String personalDataCategory, @RequestParam(name="dataDisclosureRecipientCategoryDescription",required = false)String dataDisclosureRecipientCategoryDescription,
                                    @RequestParam(name="plannedDeletionDates",required = false)String plannedDeletionDates, @RequestParam(name="securityResourcesDescription")String securityResourcesDescription,
                                    @RequestParam(name="dataDisclosureRecipients",required = false)String dataDisclosureRecipients) {
        PersonalDataStore personalDataStore = new PersonalDataStore(name,location, adminDetails, processingObjectives,targetPeopleCategoryDescription,
                personalDataCategory,dataDisclosureRecipientCategoryDescription, plannedDeletionDates,securityResourcesDescription,dataDisclosureRecipients);
        repository.save(personalDataStore);
        return personalDataStore;
    }

    @RequestMapping("/personal-data-store/list")
    @CrossOrigin
    public Iterable<PersonalDataStore> list() {
        return repository.findAll();
    }

    @RequestMapping("/personal-data-store/{id}")
    public PersonalDataStore getPersonalDataStore(@PathVariable("id") int id) {
        return repository.findById(id).orElse(null);
    }
}
