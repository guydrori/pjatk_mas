package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.*;
import java.beans.Transient;
import java.io.*;
import java.util.*;

@Entity
@Table(name="role")
public class Role implements Serializable {
    @Id
    @Column(name="ID")
    private int id;
    @Column(name="NAME")
    private String name;
    @OneToMany
    private List<User> users;
    public enum Permissions {READ,ADMIN,REPORTS_ONLY,EDIT}; //Class attribute
    @Column(name="PERMISSIONS",unique = true)
    private Permissions permissions;

    public Role (int id, String name, Permissions permissions) {
        setPermissions(permissions);
        setName(name);
        users = new ArrayList<>();
    }

    public Role (String name, Permissions permissions) {
        setPermissions(permissions);
        setName(name);
        users = new ArrayList<>();
    }

    public Role () {

    }


    public int getId() {
        return id;
    }

    public void setName(String name) {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("A name must be given!");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPermissions(Permissions permissions) {
        if (permissions == null) throw new NullPointerException("Permissions can't be null");
        this.permissions = permissions;
    }

    public Permissions getPermissions() {
        return permissions;
    }


    public void removeUser(User user) {
        if (user == null) throw new NullPointerException();
        users.remove(user);
        user.setRole(null);
    }

    public void addUser(User user) {
        if (user == null) throw new NullPointerException();
        if (user.getRole() != null && user.getRole() != this) throw new IllegalArgumentException("This user already belongs to a role!");
        users.add(user);
        if (user.getRole() == null || user.getRole() != this) {
            user.setRole(this);
        }
    }

    public List<User> getUsers() {
        return new ArrayList<>(users);
    }

    public boolean containsUser(User user) {
        return users.contains(user);
    }


    //Method overriding
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return id == role.id &&
                Objects.equals(name, role.name) &&
                permissions == role.permissions;
    }
}
