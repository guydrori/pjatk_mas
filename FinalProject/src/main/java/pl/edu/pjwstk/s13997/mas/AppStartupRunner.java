package pl.edu.pjwstk.s13997.mas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.edu.pjwstk.s13997.mas.classes.ClassAttribute;
import pl.edu.pjwstk.s13997.mas.repositories.ClassAttributeRepository;

import java.lang.reflect.Field;

@Component
public class AppStartupRunner implements CommandLineRunner {
    @Autowired
    private ClassAttributeRepository classAttributeRepository;

    @Override
    public void run(String... args) throws Exception {
        Iterable<ClassAttribute> classAttributes = classAttributeRepository.findAll();
        for (ClassAttribute attribute: classAttributes) {
            try {
                Class c = Class.forName(attribute.getClassName());
                Field field = c.getDeclaredField(attribute.getAttributeName());
                field.setAccessible(true);
                field.set(null,attribute.value);
                field.setAccessible(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
