package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Locale;
import java.util.Objects;

//Complex attribute
@Entity
@Table(name="address")
public class Address implements Serializable {
    private static final String POSTCODE_PATTERN = "\\d{2}-\\d{3}";
    @Id
    @Column(name="ID")
    @GeneratedValue
    private int id;
    @Basic(optional = false)
    @Column(name="STREET_DATA")
    private String streetData;
    @Basic(optional = false)
    @Column(name="CITY")
    private String city;
    @Basic(optional = false)
    @Column(name="POST_CODE")
    @javax.validation.constraints.Pattern(regexp=POSTCODE_PATTERN)
    private String postCode;

    public Address(String streetData, String city, String postCode) {
        setPostCode(postCode);
        setCity(city);
        setStreetData(streetData);
    }

    public Address() {
    }

    public void setPostCode(String postCode) {
        if (postCode == null || postCode.isEmpty()) throw new IllegalArgumentException("A postcode must be provided!");
        //if (!Pattern.matches(POSTCODE_PATTERN,postCode)) throw new IllegalArgumentException("The given postcode must follow the Polish format: 00-000");
        this.postCode = postCode;
    }

    public String getPostCode() { return postCode; }

    public void setCity(String city) {
        if (city == null || city.isEmpty()) throw new IllegalArgumentException("A city must be provided!");
        this.city = city.toUpperCase(Locale.forLanguageTag("pl-PL"));
    }

    public String getCity() { return city; }

    public void setStreetData (String streetData) {
        if (streetData == null || streetData.isEmpty())  throw new IllegalArgumentException("Street data must be provided!");
        this.streetData = streetData.toUpperCase(Locale.forLanguageTag("pl-PL"));
    }

    public String getStreetData() { return streetData; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(streetData, address.streetData) &&
                Objects.equals(city, address.city) &&
                Objects.equals(postCode, address.postCode);
    }
}
