package pl.edu.pjwstk.s13997.mas.classes;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.persistence.*;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Objects;
@Entity
@Table(name="user")
public class User implements Serializable {
    @Id
    @Column(name="PERSON_ID")
    private String id;

    @OneToOne(optional = false)
    @PrimaryKeyJoinColumn
    private Person whole;
    @Column(name="USERNAME",unique = true)
    private String username;
    @Column(name="PASSWORD")
    private byte[] password;
    @Column(name="SALT")
    private byte[] salt;
    @ManyToOne
    private Role role;

    public User(Person whole,Role role, String username, String password) {
        setPerson(whole);
        setRole(role);
        setPassword(password);
        setUsername(username);
    }

    public User() {

    }

    public void setUsername(String username) {
        if (username == null || username.isEmpty()) throw new IllegalArgumentException("A username must be given");
        this.username = username;
    }

    public String getUsername() { return username; }

    public static byte[] generateSalt() throws NoSuchAlgorithmException{
        // VERY important to use SecureRandom instead of just Random
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

        // Generate a 8 byte (64 bit) salt as recommended by RSA PKCS5
        byte[] salt = new byte[8];
        random.nextBytes(salt);

        return salt;
    }

    public static byte[] hashPassword(String password, byte[] salt) throws NoSuchAlgorithmException,InvalidKeySpecException {
        // PBKDF2 with SHA-1 as the hashing algorithm. Note that the NIST
        // specifically names SHA-1 as an acceptable hashing algorithm for PBKDF2
        String algorithm = "PBKDF2WithHmacSHA1";
        // SHA-1 generates 160 bit hashes, so that's what makes sense here
        int derivedKeyLength = 160;
        // Pick an iteration count that works for you. The NIST recommends at
        // least 1,000 iterations:
        // http://csrc.nist.gov/publications/nistpubs/800-132/nist-sp800-132.pdf
        // iOS 4.x reportedly uses 10,000:
        // http://blog.crackpassword.com/2010/09/smartphone-forensics-cracking-blackberry-backup-passwords/
        int iterations = 20000;

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, derivedKeyLength);

        SecretKeyFactory f = SecretKeyFactory.getInstance(algorithm);

        return f.generateSecret(spec).getEncoded();
    }

    public void setPassword(String password) {
        if (password == null || password.isEmpty()) throw new IllegalArgumentException("A password must be given");
        Boolean unique = false;
        while (!unique) {
            try {
                this.salt = generateSalt();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        unique = false;
        while (!unique) {
            try {
                this.password = hashPassword(password,this.salt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public byte[] getPassword() { return password; }

    public byte[] getSalt() { return salt; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return whole == user.whole &&
                Objects.equals(username, user.username) &&
                Arrays.equals(password, user.password) &&
                Arrays.equals(salt, user.salt);
    }

    public Person getPerson() {
        return whole;
    }

    public void setPerson(Person whole) {
        if (whole == null) {
            if (this.whole != null) {
                this.whole.removeUser(this);
            }
        } else {
            if (this.whole != whole) {
                if (this.whole != null) throw new IllegalArgumentException("This employee already belongs to a Person!");
                if (whole.getUser() != null && whole.getUser() != this) throw new IllegalArgumentException("An employee object is already assigned to this Person");
                this.whole = whole;
                if (whole.getUser() != this) {
                    whole.setUser(this);
                }
            }
        }
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        if (role == null) {
            if (this.role != null) {
                this.role.removeUser(this);
            }
        } else {
            if (this.role != role) {
                if (this.role != null) throw new IllegalArgumentException("This user already hsa a role!!");
                if (role.containsUser(this)) throw new IllegalArgumentException("A Role object is already assigned to this user");
                this.role = role;
                if (!role.containsUser(this)) {
                    role.addUser(this);
                }
            }
        }
    }

    public String getId() {
        if (whole != null) return whole.getId();
        else return null;
    }

    public String getFirstName() {
        if (whole != null) return whole.getFirstName();
        else return null;
    }

    public String getSurname() {
        if (whole != null) return whole.getSurname();
        else return null;
    }

    public String getFulllName() {
        if (whole != null) return whole.getFullName();
        else return null;
    }

    public void setPermissions(Role.Permissions permissions) {
        if (role != null) role.setPermissions(permissions);
    }

    public Role.Permissions getPermissions() {
        if (role != null) return role.getPermissions();
        else return null;
    }

    @Override
    public String toString() {
        return "Person: " + whole + " Username: " + username + " Permissions: " + role.getPermissions().name();
    }
}