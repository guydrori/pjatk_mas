package pl.edu.pjwstk.s13997.mas.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.edu.pjwstk.s13997.mas.classes.Department;
import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

public interface EmployeeRepository extends CrudRepository<Employee,String> {
    @Query(value="SELECT COUNT(*) FROM position_employees WHERE employees_person_id = ?1",nativeQuery = true)
    Long countPositions(String employeeId);
    @Query("SELECT COUNT(e) FROM Employee e WHERE e.id =?1")
    Long countById(String id);
    @Query("SELECT e FROM Employee e WHERE e.department=?1")
    Iterable<Employee> findAllByDepartment(Department department);
}
