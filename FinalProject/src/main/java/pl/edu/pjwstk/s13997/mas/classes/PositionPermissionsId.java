package pl.edu.pjwstk.s13997.mas.classes;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PositionPermissionsId implements Serializable {
    @ManyToOne
    PersonalDataStore personalDataStore;

    @ManyToOne
    Position position;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PositionPermissionsId that = (PositionPermissionsId) o;
        return Objects.equals(personalDataStore, that.personalDataStore) &&
                Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {

        return Objects.hash(personalDataStore, position);
    }
}
