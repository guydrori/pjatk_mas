package pl.edu.pjwstk.s13997.mas.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.edu.pjwstk.s13997.mas.classes.Company;
import pl.edu.pjwstk.s13997.mas.classes.Organization;
import pl.edu.pjwstk.s13997.mas.classes.SoleProprietorship;

import java.util.Optional;

public interface OrganizationRepository extends CrudRepository<Organization,Long> {
    Optional<Organization> findByName(String name);
    @Query(value="SELECT COUNT(*) AS count FROM organization_departments WHERE departments_id=?1",nativeQuery = true)
    Long getDepartmentCount(int departmentId);
    @Query(value="SELECT COUNT(*) FROM organization WHERE company_krs=?1",nativeQuery = true)
    Long getCompanyCount(String companyKRS);
    @Query(value="SELECT COUNT(*) FROM organization WHERE sole_proprietorship_id=?1",nativeQuery = true)
    Long getSoleProprietorshipCount(String soleProprietorshipId);
}
