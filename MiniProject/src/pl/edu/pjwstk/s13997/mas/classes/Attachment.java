package pl.edu.pjwstk.s13997.mas.classes;

import java.util.HashSet;
import java.util.Set;

public class Attachment {
    private String fileName;
    private String URL;
    private static Set<Attachment> extent = new HashSet<>();

    public Attachment(String fileName, String URL) {
        setFileName(fileName);
        setURL(URL);
        extent.add(this);
    }

    public void setFileName(String fileName) {
        if(fileName == null || fileName.isEmpty()) throw new IllegalArgumentException("A file name must be provided");
        if (extent.stream().anyMatch(u->u.fileName.equals(fileName))) throw new IllegalArgumentException("An attachment exists with the given filename");
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setURL(String URL) {
        if(URL == null || URL.isEmpty()) throw new IllegalArgumentException("A URL must be provided");
        if (extent.stream().anyMatch(u->u.URL.equals(URL))) throw new IllegalArgumentException("An attachment exists with the given URL");
        this.URL = URL;
    }

    public String getURL() {
        return URL;
    }
}
