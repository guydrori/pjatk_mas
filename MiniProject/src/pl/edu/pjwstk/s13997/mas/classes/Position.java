package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import java.io.Serializable;
import java.util.*;

public class Position implements Serializable {
    private String name;
    public enum Permissions {READ,MODIFY,REPORTS_ONLY};
    private static List<String> positionList = new ArrayList<>();
    private Map<PersonalDataStore,Permissions> personalDataStores;
    private Set<Employee> employees;
    private static Set<Employee> allAssociatedEmployees;

    public Position(String name,Map<PersonalDataStore,Permissions> personalDataStores,Collection<Employee> employees) {
        setName(name);
        setPersonalDataStores(personalDataStores);
        setEmployees(employees);
    }

    public Position(String name,Map<PersonalDataStore,Permissions> personalDataStores) {
        setName(name);
        setPersonalDataStores(personalDataStores);
        employees = new HashSet<>();
        allAssociatedEmployees = new HashSet<>();
    }

    public Position(String name) {
        setName(name);
        personalDataStores = new HashMap<>();
        employees = new HashSet<>();
        allAssociatedEmployees = new HashSet<>();
    }

    public void setName(String name) {
        if (positionList.contains(name)) throw new IllegalArgumentException("A position with the given name exists");
        this.name = name;
        positionList.add(name);
    }

    public String getName() { return name; }

    private void setPersonalDataStores(Map<PersonalDataStore,Permissions> personalDataStores) {
        if (personalDataStores == null) throw new NullPointerException("The Personal Data Stores map cannot be null!");
        this.personalDataStores = new HashMap<>(personalDataStores);
    }

    public void addPersonalDataStore(PersonalDataStore personalDataStore,Permissions permissions) {
        if (personalDataStore == null) throw new NullPointerException("The Personal Data Store cannot be null!");
        personalDataStores.put(personalDataStore,permissions);
        if (!personalDataStore.hasDefaultPermissions(this)) personalDataStore.addDefaultPermissions(this,permissions);
    }

    public Set<Map.Entry<PersonalDataStore,Permissions>> getDefaultPermissions() {
        return new HashSet<>(personalDataStores.entrySet());
    }

    public Set<PersonalDataStore> getPersonalDataStores() {
        return new HashSet<>(personalDataStores.keySet());
    }

    public boolean hasDefaultPermissions(PersonalDataStore personalDataStore) {
        return personalDataStores.containsKey(personalDataStore);
    }

    public void removePersonalDataStore(PersonalDataStore personalDataStore) {
        if (personalDataStore == null) throw new NullPointerException("The Personal Data Store cannot be null!");
        personalDataStores.remove(personalDataStore);
        if (personalDataStore.hasDefaultPermissions(this)) personalDataStore.removeDefaultPermissions(this);
    }

    private void setEmployees(Collection<Employee> employees) {
        if (employees == null) throw new NullPointerException("The employees collection can't be null!");
        this.employees = new HashSet<>(employees);
        allAssociatedEmployees = new HashSet<>(employees);
    }

    public Set<Employee> getEmployees() {
        return new HashSet<>(employees);
    }

    public void addEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee can't be null");
        if (allAssociatedEmployees.contains(employee)) throw new IllegalArgumentException("The given employee is already associated to a position");
        this.employees.add(employee);
        allAssociatedEmployees.add(employee);
    }

    public void removeEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee can't be null");
        if (allAssociatedEmployees.contains(employee) && !employees.contains(employee)) throw new IllegalArgumentException("Wrong position");
        this.employees.remove(employee);
        allAssociatedEmployees.remove(employee);
    }

    @Override
    public String toString() {
        return getName();
    }
}
