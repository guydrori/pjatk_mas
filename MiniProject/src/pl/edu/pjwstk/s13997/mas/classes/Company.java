package pl.edu.pjwstk.s13997.mas.classes;

import pl.edu.pjwstk.s13997.mas.classes.employee.Employee;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Company implements Serializable {
    private long NIP;
    private String name;
    private Address address; //Complex attribute
    private Set<Department> departments;
    private Set<Employee> employees;
    private static Set<Company> extent = new HashSet<>();

    public Company(long NIP, String name, Address address, Collection<Department> departments) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        setDepartments(departments);
        employees = new HashSet<>();
        extent.add(this);
    }

    public Company(long NIP, String name, Address address, Collection<Department> departments,Collection<Employee> employees) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        setDepartments(departments);
        setEmployees(employees);
        extent.add(this);
    }

    public Company(long NIP, String name, Address address) {
        setNIP(NIP);
        setAddress(address);
        setName(name);
        departments = new HashSet<>();
        employees = new HashSet<>();
        extent.add(this);
    }

    public void setAddress(Address address) {
        if (address == null) throw new NullPointerException("The address can't be null");
        this.address = address;
    }

    public Address getAddress() { return address; }

    public void setName(String name) {
        if(name == null || name.isEmpty()) throw new IllegalArgumentException("A name must be provided");
        this.name = name;
    }

    public String getName() { return name; }

    public void setNIP(long NIP) {
        if (String.valueOf(NIP).length() != 10) throw new IllegalArgumentException("NIP must have 10 digits!");
        if (extent.stream().anyMatch(c->c.NIP == NIP)) throw new IllegalArgumentException("A company with the given NIP exists");
        this.NIP = NIP;
    }

    public long getNIP() { return NIP; }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("companies.dat"));
            objectOutputStream.writeObject(Company.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("companies.dat"));
            Company.extent = (Set<Company>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Set<Company> getExtent() {
        return new HashSet<>(extent);
    }

    protected void addDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department cannot be null");
        if (extent.stream().anyMatch(c->c.departments.contains(department))) throw new IllegalArgumentException("The department is already assigned to a company, please remove it first");
        departments.add(department);
    }

    protected void removeDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department cannot be null");
        departments.remove(department);
    }

    public Set<Department> getDepartments() {
        return new HashSet<>(departments);
    }

    private void setDepartments(Collection<Department> departments) {
        if (departments != null) {
            this.departments = new HashSet<>(departments);
        }
    }

    private void setEmployees(Collection<Employee> employees) {
        if (employees != null) {
            this.employees = new HashSet<>(employees);
        }
    }

    public Set<Employee> getEmployees() {
        return new HashSet<>(employees);
    }

    public void addEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee cannot be null");
        employees.add(employee);
    }

    public void removeEmployee(Employee employee) {
        if (employee == null) throw new NullPointerException("Employee cannot be null");
        employees.remove(employee);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Company company = (Company) o;
        return NIP == company.NIP &&
                Objects.equals(name, company.name) &&
                Objects.equals(address, company.address);
    }

    @Override
    public String toString() {
        return "NIP: " + getNIP() + " Name: " + getName();
    }
}
