package pl.edu.pjwstk.s13997.mas.classes.employee;

import pl.edu.pjwstk.s13997.mas.classes.Company;
import pl.edu.pjwstk.s13997.mas.classes.Department;
import pl.edu.pjwstk.s13997.mas.classes.Position;

import java.io.*;
import java.time.LocalDate;
import java.util.*;

public class Employee implements Serializable {
    private String id;
    private String firstName;
    private String surname;
    private HashMap<String,Company> companies;
    private Department department;
    private List<LocalDate> trainingDates; //Multi-value attribute
    private Position position;
    private static Set<Employee> extent = new HashSet<>();

    Employee(String id, String firstName, String surname,Map<String,Company> companies,Department department, Position position) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        trainingDates = new LinkedList<>();
        setCompanies(companies);
        this.department = department;
        setPosition(position);
        extent.add(this);
    }

    Employee(String id, String firstName, String surname,Company company,String companyShortName,Department department, Position position) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        trainingDates = new LinkedList<>();
        setCompany(company,companyShortName);
        this.department = department;
        setPosition(position);
        extent.add(this);
    }

    Employee(String id, String firstName,String surname, Collection<LocalDate> trainingDates,Company company,String companyShortName, Department department, Position position) {
        setId(id);
        setFirstName(firstName);
        setSurname(surname);
        if (trainingDates == null) throw new NullPointerException("Training dates collection can't be null");
        this.trainingDates = new LinkedList<>(trainingDates);
        setCompany(company,companyShortName);
        this.department = department;
        setPosition(position);
        extent.add(this);
    }

    private void setId(String id) {
        if(id == null || id.isEmpty()) throw new IllegalArgumentException("An ID must be provided");
        if(extent.stream().anyMatch(e->e.id.equals(id))) throw new IllegalArgumentException("An employee with the given ID exists");
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        if(firstName == null || firstName.isEmpty()) throw new IllegalArgumentException("A first name must be provided");
        this.firstName = firstName;
    }

    private String getFirstName() { return firstName; }

    public void setSurname(String surname) {
        if(surname == null || surname.isEmpty()) throw new IllegalArgumentException("A first name must be provided");
        this.surname = surname;
    }

    private String getSurname() { return  surname; }

    //Derived attribute
    private String getFullName() { return firstName + " " + surname; }

    //Derived attribute
    public LocalDate getMostRecentTrainingDate() {
        if (trainingDates != null) return trainingDates.stream().max((d1,d2)-> {return d1.compareTo(d2);}).get();
        else return null;
    }

    public List<LocalDate> getTrainingDates() {
        if (trainingDates != null) return new LinkedList<>(trainingDates);
        else return null;
    }

    public void addTrainingDate(LocalDate date) {
        if (trainingDates == null) trainingDates = new LinkedList<>();
        trainingDates.add(date);
    }

    private void setCompanies(Map<String,Company> companies) {
        this.companies = new HashMap<>(companies);
    }

    public Map<String,Company> getCompanies() {
        return new HashMap<>(companies);
    }

    public void addCompany(String shortName, Company company) {
        if (shortName == null || shortName.isEmpty()) throw new NullPointerException("A qualifier must be provided!");
        if (company == null) throw new NullPointerException("Company musn't be null!");
        if (companies.containsKey(shortName)) throw new IllegalArgumentException("The given short name has been already used");
        company.addEmployee(this);
        companies.put(shortName,company);
    }

    public Company getCompany(String shortName) {
        if (shortName == null || shortName.isEmpty()) throw new NullPointerException("A qualifier must be provided!");
        return companies.getOrDefault(shortName,null);
    }

    private void setCompany(Company company,String shortName) {
        if (companies == null) companies = new HashMap<>();
        if (shortName == null || shortName.isEmpty()) throw new NullPointerException("A qualifier must be provided!");
        if (company == null) throw new NullPointerException("Company musn't be null!");
        company.addEmployee(this);
        companies.put(shortName,company);
    }

    public void removeCompany(String shortName) {
        if (shortName == null || shortName.isEmpty()) throw new NullPointerException("A qualifier must be provided!");
        if (!companies.containsKey(shortName)) throw new IllegalArgumentException("No company with the given shortName qualifier exists");
        companies.get(shortName).removeEmployee(this);
        companies.remove(shortName);
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        if (department == null) throw new NullPointerException("Department must not be null!");
        if (this.department != null) {
            this.department.removeEmployee(this);
            if (Department.anyContains(id)) throw new IllegalArgumentException("An employee with the given ID already exists in a department");
        }
        department.addEmployee(this);
        this.department = department;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        if (position == null) throw new NullPointerException("Position can't be null");
        if (this.position != null) {
            this.position.removeEmployee(this);
        }
        this.position = position;
        position.addEmployee(this);
    }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("employees.dat"));
            objectOutputStream.writeObject(Employee.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("employees.dat"));
            Employee.extent = (Set<Employee>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void printTrainingReport() {
        System.out.println("ID\tFirst name\tSurname\tMost recent training date\n");
        extent.forEach(e-> System.out.println(e.getId()+"\t"+e.getFirstName()+"\t"+e.getSurname()+"\t"+e.getMostRecentTrainingDate()));
    }

    public static List<Employee> exportList() {
        return new ArrayList<>(extent);
    }

    @Override
    public String toString() {
        return getFullName() +" " + getId();
    }

}
