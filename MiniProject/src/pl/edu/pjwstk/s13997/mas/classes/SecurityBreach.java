package pl.edu.pjwstk.s13997.mas.classes;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SecurityBreach implements Serializable {
    private static int idCounter = 1;
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    private int id;
    private String description;
    private String possibleConsequences;
    private String countermeasureDescription;
    public enum Status {SIGNIFICANT,INSIGNIFCANT};
    private Status status;
    private LocalDateTime creationTime;
    private static Set<SecurityBreach> extent = new HashSet<>();

    public SecurityBreach(int id, String description, String possibleConsquences, String countermeasureDescription, Status status, LocalDateTime creationTime) {
        setCreationTime(creationTime);
        setStatus(status);
        setCountermeasureDescription(countermeasureDescription);
        setPossibleConsequences(possibleConsquences);
        setDescription(description);
        setId(id);
        extent.add(this);
    }

    public SecurityBreach(String description, String possibleConsquences, String countermeasureDescription, Status status, LocalDateTime creationTime) {
        setCreationTime(creationTime);
        setStatus(status);
        setCountermeasureDescription(countermeasureDescription);
        setPossibleConsequences(possibleConsquences);
        setDescription(description);
        id = idCounter;
        idCounter++;
        extent.add(this);
    }

    public SecurityBreach(String description, String possibleConsquences, String countermeasureDescription, Status status) {
        creationTime = LocalDateTime.now();
        setCountermeasureDescription(countermeasureDescription);
        setPossibleConsequences(possibleConsquences);
        setDescription(description);
        setId(id);
        extent.add(this);
    }

    private void setId(int id) {
        if (id >= idCounter) {
            this.id = id;
            if (id == idCounter) idCounter++;
            else idCounter = id + 1;
        } else {
            throw new IllegalArgumentException("The given ID has been already assigned or it is invalid");
        }
    }

    public int getId() {
        return id;
    }


    public void setDescription(String description) {
        if(description == null || description.isEmpty()) throw new IllegalArgumentException("A description must be provided");
        this.description = description;
    }

    public String getDescription() { return description; }

    public void setPossibleConsequences(String possibleConsequences) {
        if(possibleConsequences == null || possibleConsequences.isEmpty()) throw new IllegalArgumentException("A description of possible consequences must be provided");
        this.possibleConsequences = possibleConsequences;
    }

    public String getPossibleConsequences() { return possibleConsequences; }

    public void setCountermeasureDescription(String countermeasureDescription) {
        if(countermeasureDescription == null || countermeasureDescription.isEmpty()) throw new IllegalArgumentException("A description of countermeasures must be provided");
        this.countermeasureDescription = countermeasureDescription;
    }

    public String getCountermeasureDescription() { return countermeasureDescription; }

    public void setCreationTime(LocalDateTime creationTime) {
        if (creationTime == null) throw new NullPointerException("Creation time must not be null");
        this.creationTime = creationTime;
    }

    public LocalDateTime getCreationTime() { return creationTime; }

    public void setStatus(Status status) {
        if (status == null) throw new NullPointerException("Status must not be null");
        this.status = status;
    }

    public Status getStatus() { return status; }

    //Class method
    public static SecurityBreach exportBreach(int id) {
        return extent.stream().filter(sb->sb.getId() == id).collect(Collectors.toList()).get(0);
    }

    //Method overloading
    public static List<SecurityBreach> exportAllBreaches() {
        return new LinkedList<>(extent);
    }

    public static List<SecurityBreach> exportAllBreaches(LocalDate entryDate) {
        return extent.stream().filter(sb->{
            LocalDateTime creationTime = sb.creationTime;
            return creationTime.getDayOfMonth() == entryDate.getDayOfMonth() &&
                    creationTime.getMonth() == entryDate.getMonth() &&
                    creationTime.getYear() == entryDate.getYear();
        }).collect(Collectors.toList());
    }

    public void printBreach() {
        System.out.println("ID\tDescription\tPossible consequences\tCountermeasure Description\tStatus\tcreationTime\n");
        extent.forEach(sb-> System.out.println(sb.getId()+"\t"+sb.getDescription()+"\t"+sb.getPossibleConsequences()+"\t"+sb.getCreationTime()+"\t"+sb.getStatus().name()+"\t"+sb.getCreationTime().format(DATE_FORMATTER)));
    }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("breaches.dat"));
            objectOutputStream.writeObject(SecurityBreach.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("breaches.dat"));
            SecurityBreach.extent = (Set<SecurityBreach>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {

        return "ID: " + getId() + " Description: " + getDescription() + " Status: " + getStatus() + " Creation Date: " + getCreationTime().format(DATE_FORMATTER);
    }
}
