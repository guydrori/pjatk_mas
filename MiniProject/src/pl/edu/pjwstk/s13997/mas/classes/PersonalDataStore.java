package pl.edu.pjwstk.s13997.mas.classes;

import java.io.*;
import java.util.*;

public class PersonalDataStore implements Serializable {
    private static int idCounter = 1;
    private int id;
    private String name;
    private String location; //Optional attribute
    private String adminDetails;
    private String processingObjectives;
    private String targetPeopleCategoryDescription;
    private String personalDataCategory;
    private String dataDisclosureRecipientCategoryDescription;
    private String plannedDeletionDates;
    private String securityResourcesDescription;
    private List<String> dataDisclosureRecipients; //Multi-value attribute
    private Map<Position,Position.Permissions> defaultPermissions;
    private static Set<PersonalDataStore> extent = new HashSet<>();

    public PersonalDataStore(int id, String name, String location, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String dataDisclosureRecipientCategoryDescription, String plannedDeletionDates, String securityResourcesDescription, Collection<String> dataDisclosureRecipients, Map<Position,Position.Permissions> defaultPermissions) {
        setId(id);
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        if (dataDisclosureRecipients != null) this.dataDisclosureRecipients = new ArrayList<>(dataDisclosureRecipients);
        setDefaultPermissions(defaultPermissions);
        extent.add(this);
    }

    public PersonalDataStore(int id, String name, String location, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String dataDisclosureRecipientCategoryDescription, String plannedDeletionDates, String securityResourcesDescription, Collection<String> dataDisclosureRecipients) {
        setId(id);
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        if (dataDisclosureRecipients != null) this.dataDisclosureRecipients = new ArrayList<>(dataDisclosureRecipients);
        defaultPermissions = new HashMap<>();
        extent.add(this);
    }

    public PersonalDataStore(String name, String location, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String dataDisclosureRecipientCategoryDescription, String plannedDeletionDates, String securityResourcesDescription) {
        id = idCounter;
        idCounter++;
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        defaultPermissions = new HashMap<>();
        extent.add(this);
    }

    public PersonalDataStore(String name, String adminDetails, String processingObjectives, String targetPeopleCategoryDescription, String personalDataCategory, String securityResourcesDescription) {
        id = idCounter;
        idCounter++;
        setName(name);
        setLocation(location);
        setAdminDetails(adminDetails);
        setProcessingObjectives(processingObjectives);
        setTargetPeopleCategoryDescription(targetPeopleCategoryDescription);
        setPersonalDataCategory(personalDataCategory);
        setDataDisclosureRecipientCategoryDescription(dataDisclosureRecipientCategoryDescription);
        setPlannedDeletionDates(plannedDeletionDates);
        setSecurityResourcesDescription(securityResourcesDescription);
        defaultPermissions = new HashMap<>();
        extent.add(this);
    }

    private void setId(int id) {
        if (id >= idCounter) {
            this.id = id;
            if (id == idCounter) idCounter++;
            else idCounter = id + 1;
        } else {
            throw new IllegalArgumentException("The given ID has been already assigned or it is invalid");
        }
    }

    public int getId() {
        return id;
    }

    //Optional attribute
    public void setLocation(String location) {
        if(location == null || location.isEmpty()) this.location = "N/A";
        else this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setAdminDetails(String adminDetails) {
        if(adminDetails == null || adminDetails.isEmpty()) throw new IllegalArgumentException("Admin details must be provided");
        this.adminDetails = adminDetails;
    }

    public String getAdminDetails() {
        return adminDetails;
    }

    public void setProcessingObjectives(String processingObjectives) {
        if(processingObjectives == null || processingObjectives.isEmpty()) throw new IllegalArgumentException("Processing objectives must be provided");
        this.processingObjectives = processingObjectives;
    }

    public String getProcessingObjectives() {
        return processingObjectives;
    }

    public void setTargetPeopleCategoryDescription(String targetPeopleCategoryDescription) {
        if(targetPeopleCategoryDescription == null || targetPeopleCategoryDescription.isEmpty()) throw new IllegalArgumentException("The category of target people must be described");
        this.targetPeopleCategoryDescription = targetPeopleCategoryDescription;
    }

    public String getTargetPeopleCategoryDescription() {
        return targetPeopleCategoryDescription;
    }

    public void setPersonalDataCategory(String personalDataCategory) {
        if(personalDataCategory == null || personalDataCategory.isEmpty()) throw new IllegalArgumentException("The personal data category must be provided");
        this.personalDataCategory = personalDataCategory;
    }

    public String getPersonalDataCategory() {
        return personalDataCategory;
    }

    public void setDataDisclosureRecipientCategoryDescription(String dataDisclosureRecipientCategoryDescription) {
        if(dataDisclosureRecipientCategoryDescription == null || dataDisclosureRecipientCategoryDescription.isEmpty()) this.dataDisclosureRecipientCategoryDescription = "N/A";
        else this.dataDisclosureRecipientCategoryDescription = dataDisclosureRecipientCategoryDescription;
    }

    public String getDataDisclosureRecipientCategoryDescription() {
        return dataDisclosureRecipientCategoryDescription;
    }

    public void setPlannedDeletionDates(String plannedDeletionDates) {
        if(plannedDeletionDates == null || plannedDeletionDates.isEmpty()) this.plannedDeletionDates = "N/A";
        else this.plannedDeletionDates = plannedDeletionDates;
    }

    public String getPlannedDeletionDates() {
        return plannedDeletionDates;
    }

    public void setSecurityResourcesDescription(String securityResourcesDescription) {
        if(securityResourcesDescription == null || securityResourcesDescription.isEmpty()) throw new IllegalArgumentException("Security resources must be described");
        this.securityResourcesDescription = securityResourcesDescription;
    }

    public String getSecurityResourcesDescription() {
        return securityResourcesDescription;
    }

    public void addDataDisclosureRecipient(String recipient) { dataDisclosureRecipients.add(recipient);}

    public boolean removeDataDisclosureRecipient(String recipient) { return dataDisclosureRecipients.remove(recipient); }

    public List<String> getDataDisclosureRecipients() {
        return new LinkedList<>(dataDisclosureRecipients);
    }

    private void setDefaultPermissions(Map<Position,Position.Permissions> defaultPermissions) {
        this.defaultPermissions = new HashMap<>(defaultPermissions);
    }

    public void addDefaultPermissions(Position position, Position.Permissions permissions) {
        if (position == null) throw new NullPointerException("Position can't be null");
        defaultPermissions.put(position,permissions);
        if (!position.hasDefaultPermissions(this)) position.addPersonalDataStore(this,permissions);
    }

    public void removeDefaultPermissions(Position position) {
        if (position == null) throw new NullPointerException("Position can't be null");
        defaultPermissions.remove(position);
        if (position.hasDefaultPermissions(this)) position.removePersonalDataStore(this);
    }

    public Set<Map.Entry<Position,Position.Permissions>> getDefaultPermissionSet() {
        return new HashSet<>(defaultPermissions.entrySet());
    }

    public Set<Position> getPositionSet() {
        return new HashSet<>(defaultPermissions.keySet());
    }

    public Position.Permissions getDefaultPermissions(Position position) {
        if (position == null) throw new NullPointerException("Position can't be null");
        if (!defaultPermissions.containsKey(position)) throw new IllegalArgumentException("No default permissions are defined for the given position");
        return defaultPermissions.get(position);
    }

    public boolean hasDefaultPermissions(Position position) {
        return defaultPermissions.containsKey(position);
    }

    public void setName(String name) {
        if (name == null || name.isEmpty()) throw new IllegalArgumentException("Name must be provided!");
        this.name = name;
    }

    public String getName() { return name; }

    public static void saveExtent() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("dataStores.dat"));
            objectOutputStream.writeObject(PersonalDataStore.extent);
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadExtent() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("dataStores.dat"));
            PersonalDataStore.extent = (Set<PersonalDataStore>) objectInputStream.readObject();
            objectInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return getName();
    }
}
