package overlapping;

public enum PersonType {
	STUDENT,
	DOCTOR,
	PATIENT
}
