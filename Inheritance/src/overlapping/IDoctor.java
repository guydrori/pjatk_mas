package overlapping;

public interface IDoctor {
	
	public void setSalary(double salary);
	public double getSalary();
	
	public void cureDisease(IPatient patient);
}
