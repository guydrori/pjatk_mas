package overlapping;

import java.util.EnumSet;

public class Person implements IPatient, IStudent, IDoctor {
	private EnumSet<PersonType> personTypes;
	
	private double scholarship;
	private double salary;
	private String diseaseDesc;


	public Person(double scholarship, double salary, String diseaseDesc, EnumSet<PersonType> pT){
		if(pT != null){
			if(!pT.isEmpty()){
				this.scholarship = scholarship;
				this.salary = salary;
				this.diseaseDesc = diseaseDesc;
				
			}
			else throw new RuntimeException("Person types is empty");
		}
		else throw new RuntimeException("Person types is null");
	}

	public double getScholarship() {
		return scholarship;
	}

	public void setScholarship(double scholarship) {
		this.scholarship = scholarship;
	}
	
	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	
	public String getDiseaseDesc() {
		return diseaseDesc;
	}

	public void setDiseaseDesc(String diseaseDesc) {
		this.diseaseDesc = diseaseDesc;
	}
	

	@Override
	public void cureDisease(IPatient patient) {
		if(personTypes.contains(PersonType.DOCTOR)){
			// TODO
		}
		else throw new RuntimeException("Person is not a doctor");
	}
	
	public static void main(String[] args) {
		EnumSet<PersonType> of = EnumSet.of(PersonType.DOCTOR, PersonType.STUDENT);
		
	}

}
