package dynamic;

import java.util.EnumSet;

public class Person implements IPatient, IStudent, IDoctor {
	private PersonType personType;
	
	private Double scholarship;
	private Double salary;
	private String diseaseDesc;


	public Person(Double scholarship, Double salary, String diseaseDesc, PersonType pT){
		if(pT != null){
			this.scholarship = scholarship;
			this.salary = salary;
			this.diseaseDesc = diseaseDesc;
		}
		if(PersonType.DOCTOR.equals(pT)) {
			if(salary == null) {
				throw new RuntimeException("salary is mandatory for a Doctor");
			}
		}
		else throw new RuntimeException("Person types is null");
	}
	
	public void becomeDoctor(double salary) {
		this.salary = salary;
		this.scholarship = null;
		this.diseaseDesc = null;
		this.personType = PersonType.DOCTOR;
	}
	public void becomePatient(String diseaseDesc) {
		this.diseaseDesc = diseaseDesc;
		this.salary = null;
		this.scholarship = null;
		this.personType = PersonType.PATIENT;
	}
	public void becomeStudent(double scholarship) {
		this.scholarship = scholarship;
		this.salary  = null;
		this.diseaseDesc = null;
		this.personType = PersonType.STUDENT;
	}

	public double getScholarship() {
		if (personType != PersonType.STUDENT) throw new RuntimeException("The instance is not of type Student!");
		return scholarship;
	}

	public void setScholarship(double scholarship) {
		if (personType != PersonType.STUDENT) throw new RuntimeException("The instance is not of type Student!");
		this.scholarship = scholarship;
	}
	
	public double getSalary() {
		if (personType != PersonType.DOCTOR) throw new RuntimeException("The instance is not of type Doctor!");
		return salary;
	}

	public void setSalary(double salary) {
		if (personType != PersonType.DOCTOR) throw new RuntimeException("The instance is not of type Doctor!");
		this.salary = salary;
	}
	
	
	public String getDiseaseDesc() {
		if (personType != PersonType.PATIENT) throw new RuntimeException("The instance is not of type Patient!");
		return diseaseDesc;
	}

	public void setDiseaseDesc(String diseaseDesc) {
		if (personType != PersonType.PATIENT) throw new RuntimeException("The instance is not of type Patient!");
		this.diseaseDesc = diseaseDesc;
	}
	

	@Override
	public void cureDisease(IPatient patient) {
		if(PersonType.DOCTOR.equals(personType)) {
			// TODO
		}
		else throw new RuntimeException("Person is not a doctor");
	}
	
	public static void main(String[] args) {
		EnumSet<PersonType> of = EnumSet.of(PersonType.DOCTOR, PersonType.STUDENT);
//		IDoctor doc = new Person(scholarship, salary, diseaseDesc, pT)
	}

}
