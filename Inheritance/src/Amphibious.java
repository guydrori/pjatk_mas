public class Amphibious extends Boat implements Car {
	public CarHelper carHelper = new CarHelper();
	
	public void complexMethod() {
		carHelper.complexMethod();
	}

	public int getWheelNumber() {
		return carHelper.getWheelNumber();
	}

	public void setWheelNumber(int numberOfWheels) {
		carHelper.setWheelNumber(numberOfWheels);
	}
	
	
}
