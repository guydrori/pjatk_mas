package abstractClass;

import java.util.ArrayList;
import java.util.Collection;

public class Main {

	public static void main(String[] args) {
		Collection<Vehicle> vehicles = new ArrayList<>();
		vehicles.add(new SportCar(4));
		vehicles.add(new Bus(6));
		vehicles.add(new Car(4));	
		
		for(Vehicle v : vehicles) {
			v.drive();
		}
		
	}

}
