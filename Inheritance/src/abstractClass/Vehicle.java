package abstractClass;

public abstract class Vehicle {
	private int wheels;
	
	public Vehicle(int wheels) {
		this.wheels = wheels;
	}

	public abstract void drive();

	public int getWheels() {
		return wheels;
	}

	public void setWheels(int wheels) {
		this.wheels = wheels;
	}
	

}
