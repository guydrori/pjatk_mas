package abstractClass;

public class Bus extends Vehicle {

	public Bus(int wheels) {
		super(wheels);
		
	}

	@Override
	public void drive() {
		System.out.println("Bus.drive()");
	}

}
